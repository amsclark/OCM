import json
import sys

def format_semgrep_output(file_path):
    with open(file_path, 'r') as f:
        data = json.load(f)
        if 'results' in data:
            for result in data['results']:
                print(f"File: {result['path']}")
                print(f"  Line: {result['start']['line']}")
                print(f"  Rule: {result['check_id']}")
                print(f"  Message: {result['extra']['message']}")
                print("  Code snippet:")
                print(f"  {result['extra']['lines']}")
                print("-" * 80)
        else:
            print("No results found.")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python format_semgrep.py <path_to_json_file>")
    else:
        format_semgrep_output(sys.argv[1])