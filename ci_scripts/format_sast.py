# ci_scripts/format_sast.py
import json
import sys

def format_sast(json_file):
    try:
        with open(json_file, 'r') as f:
            data = json.load(f)
        
        print("\nSAST Report\n" + "="*40)
        
        if not data or 'vulnerabilities' not in data:
            print("No vulnerabilities found or invalid SAST report format.")
            return
        
        for vulnerability in data['vulnerabilities']:
            print(f"Vulnerability: {vulnerability.get('message', 'N/A')}")
            print(f"File: {vulnerability.get('file', 'N/A')}")
            print(f"Line: {vulnerability.get('line', 'N/A')}")
            print(f"Severity: {vulnerability.get('severity', 'N/A')}")
            print(f"Description: {vulnerability.get('description', 'N/A')}")
            print("-" * 40)
    except Exception as e:
        print(f"Error reading or processing JSON file: {e}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python format_sast.py <json_file>")
        sys.exit(1)

    format_sast(sys.argv[1])
