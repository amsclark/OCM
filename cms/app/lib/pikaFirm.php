<?php

/**********************************/

/* Pika CMS (C) 2009 Aaron Worley */
/* http://pikasoftware.com        */
/**********************************/

require_once('plBase.php');

/**
* pikaFirm - Interface to the firms table - extends plBase
* replaces deprecated pb_firms_lib.php
*
* @author Matthew Friedlander <amworley@pikasoftware.com>;
* @version 1.0
* @package Danio
*/

class pikaFirm extends plBase
{
    public function __construct($firm_id = null)
    {
        $this->db_table = 'firms';
        parent::__construct($firm_id);
        return true;
    }

    public static function fetchFirmsPbaList($firm_id)
    {
        if ($firm_id) {
            $safe_firm_id = DB::escapeString($firm_id);

            $params = [];
            $params[] = $safe_firm_id;
            $sql = "SELECT * FROM pb_attorneys
					WHERE 1 
					AND pb_attorneys.firm_id=?
					ORDER BY pb_attorneys.last_name, pb_attorneys.first_name";
            //echo $sql;
            $result = DB::preparedQuery($sql, $params) or trigger_error("SQL: " . $sql . " Error: " . DB::error());
            return $result;
        }

        return false;
    }



    public static function fetchFirmsCaseList($firm_id)
    {
        if ($firm_id) {
            $safe_firm_id = DB::escapeString($firm_id);

            $params = [];
            $params[] = $safe_firm_id;
            $params[] = $safe_firm_id;
            $params[] = $safe_firm_id;
            $sql = "(SELECT cases.* FROM cases 
					JOIN pb_attorneys ON cases.pba_id1 = pb_attorneys.pba_id
					JOIN firms ON pb_attorneys.firm_id = firms.firm_id 
					WHERE 1 AND firms.firm_id = ?)
					UNION 
					(SELECT cases.* FROM cases 
					JOIN pb_attorneys ON cases.pba_id2 = pb_attorneys.pba_id
					JOIN firms ON pb_attorneys.firm_id = firms.firm_id 
					WHERE 1 AND firms.firm_id = ?)
					UNION 
					(SELECT cases.* FROM cases 
					JOIN pb_attorneys ON cases.pba_id3 = pb_attorneys.pba_id
					JOIN firms ON pb_attorneys.firm_id = firms.firm_id 
					WHERE 1 AND firms.firm_id = ?)
                    ORDER BY open_date DESC;";
            //echo $sql;
            $result = DB::preparedQuery($sql, $params) or trigger_error("SQL: " . $sql . " Error: " . DB::error());
            return $result;
        }

        return false;
    }

    public static function fetchFirmsLastCase($firm_id)
    {
        $last_case = '';
        if ($firm_id) {
            $safe_firm_id = DB::escapeString($firm_id);

            $params = [];
            $params[] = $safe_firm_id;
            $params[] = $safe_firm_id;
            $params[] = $safe_firm_id;
            $sql = "(SELECT open_date AS last_case FROM cases
					JOIN pb_attorneys ON cases.pba_id1 = pb_attorneys.pba_id
					JOIN firms ON pb_attorneys.firm_id = firms.firm_id 
					WHERE 1 AND firms.firm_id = ?)
					UNION 
					(SELECT open_date AS last_case FROM cases 
					JOIN pb_attorneys ON cases.pba_id2 = pb_attorneys.pba_id
					JOIN firms ON pb_attorneys.firm_id = firms.firm_id 
					WHERE 1 AND firms.firm_id = ?)
					UNION 
					(SELECT open_date AS last_case FROM cases 
					JOIN pb_attorneys ON cases.pba_id3 = pb_attorneys.pba_id
					JOIN firms ON pb_attorneys.firm_id = firms.firm_id 
					WHERE 1 AND firms.firm_id = ?)
					ORDER BY last_case DESC LIMIT 1;";

            //echo $sql;
            $result = DB::preparedQuery($sql, $params) or trigger_error("SQL: " . $sql . " Error: " . DB::error());
            if (DBResult::numRows($result) == 1) {
                $row = DBResult::fetchRow($result);
                $last_case = $row['last_case'];
            }
        }

        return $last_case;
    }

    // 2015-11-19 MSH added function
    public static function fetchFirmsLastActivity($firm_id)
    {
        $last_act = '';
        if ($firm_id) {
            $safe_firm_id = DB::escapeString($firm_id);
            $params = [];
            $params[] = $safe_firm_id;
            $sql = "SELECT MAX(t.act_date) AS last_act FROM 
					(SELECT act_id, act_date, pba_id FROM activities
						WHERE pba_id IS NOT NULL AND case_id IS NULL) AS t
					JOIN pb_attorneys ON t.pba_id = pb_attorneys.pba_id
					WHERE 1 AND pb_attorneys.firm_id = ?";

            //echo $sql;
            $result = DB::preparedQuery($sql, $params) or trigger_error("SQL: " . $sql . " Error: " . DB::error());
            if (DBResult::numRows($result) == 1) {
                $row = DBResult::fetchRow($result);
                $last_act = $row['last_act'];
            }
        }

        return $last_act;
    }

    // 2015-12-22 MSH added function
    public static function fetchFirmsActivityList($firm_id)
    {
        if ($firm_id) {
            $safe_firm_id = DB::escapeString($firm_id);

            $sql = "SELECT MAX(act_date) as latest, a.om_code FROM activities a
                    JOIN pb_attorneys ON a.pba_id = pb_attorneys.pba_id
					JOIN firms ON pb_attorneys.firm_id = firms.firm_id 
                    WHERE 1 AND a.pba_id IS NOT NULL AND a.case_id IS NULL
					AND pb_attorneys.firm_id = '{$safe_firm_id}'
                    GROUP BY a.om_code
                    ORDER BY MAX(act_date) DESC;";

            //echo $sql;
            $result = DB::query($sql) or trigger_error("SQL: " . $sql . " Error: " . DB::error());
            return $result;
        }

        return false;
    }

    public static function fetchFirmNotesList($firm_id, $order = 'ASC', $list_length = 50, $first_row = 0, &$row_count, &$total_hours)
    {
        $clean_order = DB::escapeString($order);
        if ($firm_id) {
            $safe_firm_id = DB::escapeString($firm_id);

            $sql = "SELECT COUNT(*) AS count
				FROM activities
				WHERE firm_id='{$safe_firm_id}';";

            $result = DB::query($sql) or trigger_error("SQL: " . $sql . " Error: " . DB::error());
            $row = DBResult::fetchRow($result);
            $row_count = $row['count'];

            $sql = "SELECT SUM(hours) AS hours
                FROM activities
                WHERE firm_id='{$safe_firm_id}';";

            $result = DB::query($sql) or trigger_error("SQL: " . $sql . " Error: " . DB::error());
            $row = DBResult::fetchRow($result);
            $total_hours = $row['hours'];

            $sql = "SELECT activities.*
                FROM activities
                WHERE firm_id='{$safe_firm_id}'
                ORDER BY act_date {$clean_order}, act_time {$clean_order}, last_changed {$clean_order}";

            if ($first_row && $list_length) {
                $sql .= " LIMIT $first_row, $list_length";
            } elseif ($list_length) {
                $sql .= " LIMIT $list_length";
            }

            $result = DB::query($sql) or trigger_error("SQL: " . $sql . " Error: " . DB::error());
            return $result;
        }

        return false;
    }

    public static function fetchFirm($filter, &$firm_count, $first_row = "", $list_length = "")
    {
        return self::getFirmsDB($filter, $firm_count, $first_row, $list_length);
    }

    public static function getFirmsDB($filter = array(), &$row_count = 0, $first_row = "", $list_length = "")
    {
        foreach ($filter as $key => $value) {
            $filter[$key] = DB::escapeString($filter[$key]);
        }

        $sql_filter = $sql_limit = "";
        if ($first_row && $list_length) {
            $sql_limit = " LIMIT $first_row, $list_length";
        } elseif ($list_length) {
            $sql_limit = " LIMIT $list_length";
        }

        // handle filter options
        if (isset($filter['county']) && $filter['county']) {
            $sql_filter .= " AND county LIKE '%{$filter['county']}%'";
        }
        if (isset($filter['firm_name']) && $filter['firm_name']) {
            $sql_filter .= " AND firm_name LIKE '%{$filter['firm_name']}%'";
        }
        if (isset($filter['firm_size']) && $filter['firm_size']) {
            $sql_filter .= " AND firm_size LIKE '%{$filter['firm_size']}%'";
        }
        $sql = "SELECT COUNT(*) as nbr 
				FROM firms 
				WHERE 1" . $sql_filter;

        $result = DB::query($sql) or trigger_error("SQL: " . $sql . " Error: " . DB::error());

        if (DBResult::numRows($result) < 1) {
            $row_count = 0;
        } else {
            $row = DBResult::fetchRow($result);
            $row_count = $row['nbr'];
        }

        $sql = "SELECT * 
				FROM firms 
				WHERE 1" . $sql_filter .
                " ORDER BY firm_name ASC" .
                $sql_limit;
        //echo $sql;

        $result = DB::query($sql) or trigger_error("SQL: " . $sql . " Error: " . DB::error());
        return $result;
    }

    public static function fetchFirmsArray()
    {
        $result = self::getFirmsDB(array());
        $a = array();
        while ($row = DBResult::fetchRow($result)) {
            $firm_name = 'No Name (' . $row['firm_id'] . ')';

            $a[$row['firm_id']] = "{$row['firm_name']}";
        }

        return $a;
    }

    public static function fetchFirmsAArray()
    {
        $result = self::getFirmsDB(array());
        $a = array();
        while ($row = DBResult::fetchRow($result)) {
            $a[$row['firm_id']] = "{$row['firm_name']}  --- {$row['address']}  {$row['city']}  {$row['zip']}";
        }

        return $a;
    }

    public static function fetchFirmsADArray()
    {
        $result = self::getFirmsDB(array());
        $a = array();
        while ($row = DBResult::fetchRow($result)) {
            $a[$row['firm_id']] = "{$row['address']}  {$row['city']}  {$row['state']}  {$row['zip']}";
        }

        return $a;
    }
}
