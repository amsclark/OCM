<?php

require_once('plCsvReportTable.php');

/**
* plCsvReport - Creates an CSV report display containing one or more instances
* of plCsvReportTable.  Creates first table automatically - for legacy support.
*
* @author Aaron Worley <amworley@pikasoftware.com>;
* @version 1.0
* @package Danio
*/
class plCsvReport
{
    public $tables = array();
    public $current_table_index;
    public $title = '';
    public $parameters = array();

    public function __construct()
    {
        $this->title = 'A Report';
        // 2013-08-13 AMW - Removed =& for compatibility with PHP 5.3.
        $this->tables[] = new plCsvReportTable();
        $this->current_table_index = '0';
    }

    public function add_table()
    {
        // 2013-08-13 AMW - Removed =& for compatibility with PHP 5.3.
        $this->tables[] = new plCsvReportTable();
        $this->current_table_index += 1;
    }

    public function add_row($a = array())
    {
        $this->tables[$this->current_table_index]->add_row($a);
    }

    public function add_parameter($name, $parameter)
    {
        $parameters = $this->parameters;
        $parameters[] = array('name' => $name,'param' => $parameter);
        $this->parameters = $parameters;
    }

    public function set_sql($sql)
    {
    }

    public function set_header($a = array())
    {
        $this->tables[$this->current_table_index]->set_header($a);
    }

    public function set_footer($footer = '')
    {
    }

    public function set_title($title)
    {
        $this->title = strip_tags($title);
    }

    public function set_table_title($title)
    {
        $this->tables[$this->current_table_index]->set_title($title);
    }

    public function display_row_count($value)
    {
    }

    public function display($headers_first_row = false)
    {
        if ($header_first_row == false) {
            $buffer = '"' . addslashes($this->title) . '",' . "\n";
        }
        foreach ($this->parameters as $parameter) {
            if (isset($parameter['name']) && $parameter['name'] && isset($parameter['param'])) {
                $buffer .= '"' . addslashes($parameter['name']) . ': ' . addslashes($parameter['param']) . '",' . "\n";
            }
        }

        for ($i = 0; $i < count($this->tables); $i++) {
            $buffer .= $this->tables[$i]->build();
        }
        /* Firefox changed something in July 2024 and these headers no longer work to trigger a download. It now displays the csv content in-browser.
        header("Pragma: public");
        header("Cache-Control: cache, must-revalidate");
        header("Content-type: application/force-download");
        header("Content-Type: text/x-comma-separated-values");
        */

        // New headers July 2024
        // Disable PHP output buffering and compression
        ini_set('output_buffering', 'off');
        ini_set('zlib.output_compression', 'off');

        if (ob_get_length()) {
            ob_end_clean();
        }

        // Set headers
        header('Content-Encoding: Identity');
        header('Content-Disposition: attachment; filename="Mega Report.csv"');
        header('Content-Type: text/x-comma-separated-values;charset=UTF-8');
        header('Content-Length: ' . strlen($buffer));
        header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
        header('Pragma: no-cache');
        header('Expires: Thu, 19 Nov 1981 08:52:00 GMT');

        /*
        if (pl_settings_get("enable_compression") == false)
        {
            header("Content-Length: " . mb_strlen($buffer));
        }
        */


        // AMW 2013-10-16 - Workaround for new Chrome/CSV behavior.
        // Alex Clark - 2025-07-25
        /* Firefox and chrome behavior changed. this no longer is needed.
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false  ||
                strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== false)
        {
            header("Content-Disposition: attachment; filename=\"{$this->title}.csv\"");
        }

        else
        {
            header("Content-Disposition: inline; filename=\"{$this->title}.csv\"");
        }
        */
        if ($headers_first_row == true) {
            $buffer = preg_replace('/^\"\",/', '', $buffer);
            echo trim($buffer);
        } else {
            echo $buffer;
        }
        exit();
    }
}
