alter table cases add column unread_sms tinyint(4) default NULL;
alter table users add column totp_enabled tinyint(4);
alter table users add column totp_secret varchar(128);
CREATE TABLE `menu_totp_enabled` (
  `value` tinyint(4) NOT NULL DEFAULT '0',
  `label` char(65) NOT NULL DEFAULT '',
  `menu_order` tinyint(4) NOT NULL DEFAULT '0',
  KEY `label` (`label`),
  KEY `val` (`value`),
  KEY `menu_order` (`menu_order`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


INSERT INTO `menu_totp_enabled` VALUES (1,'Yes',0),(0,'No',1),(2,'Reset',2);
CREATE TABLE `pw_reset_tokens` (
  `token_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(25) NOT NULL DEFAULT '',
  `token` varchar(128) NOT NULL DEFAULT '',
  `used` tinyint(4) NOT NULL DEFAULT '0',
  `token_expire` int(11) DEFAULT '0'
) ENGINE=InnoDB;