<?php

chdir('../');
require_once('pika-danio.php');

pika_init();


if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $qrCodeData = $_POST["qrcode"];
    $qrCodeBase64 = preg_replace('/^data:image\/png;base64,/', '', $qrCodeData);

    $enrollingUserEmail = $_POST["email"];
    $subject = "OCM MFA Enrollment Email";
    // $qrcodehtml = '<html><body>Please scan the below qr code with Google Authenticator<br><br><img style="display: block;" src="' . $_POST["qrcode"] . '"></body></html>';
    $qrcodehtml = '<html><body>'
                . 'Please scan the QR code below with Google Authenticator:<br><br>'
                . '<img src="cid:qrcode.png" alt="QR Code">'
                . '</body></html>';
    //$qrcodehtml = json_encode($qrcodehtml);


    //var_dump(pl_settings_get('sparkpost_from_address'));
    //var_dump(pl_settings_get('sparkpost_api_key'));
    send_MFA_email($enrollingUserEmail, $subject, $qrcodehtml, $qrCodeBase64);
}


function send_MFA_email($enrollingUserEmail, $subject, $qrcodehtml, $qrCodeBase64)
{

    //$data_string='{"options": {"sandbox": false, "open_tracking": false, "click_tracking": false}, "content": {"from": "'
    //. pl_settings_get('sparkpost_from_address')
    //. '", "subject": "' . $subject . '", "html":' . $qrcodehtml
    //. '}, "recipients": [{"address": "' . $enrollingUserEmail . '"}]}';
    //echo "data string: \r\n";
    //var_dump($data_string);
    //echo "\r\n";

    $data_string = json_encode([
          "options" => [
              "sandbox" => false,
              "open_tracking" => false,
              "click_tracking" => false,
          ],
          "content" => [
              "from" => pl_settings_get('sparkpost_from_address'),
              "subject" => $subject,
              "html" => $qrcodehtml,
              "inline_images" => [
                  [
                      "name" => "qrcode.png", // Name of the attachment
                      "type" => "image/png", // MIME type
                      "data" => $qrCodeBase64, // Base64 encoded image data
                  ]
              ]
          ],
          "recipients" => [
              ["address" => $enrollingUserEmail],
          ],
      ]);

    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, 'https://api.sparkpost.com/api/v1/transmissions');
    curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($c, CURLOPT_TIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSLVERSION, 6);
    curl_setopt($c, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($c, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: ' . pl_settings_get('sparkpost_api_key')]);
    $status_code = curl_getinfo($c, CURLINFO_HTTP_CODE);
    echo "status code: \r\n";
    var_dump($status_code);
    echo "\r\n";
    $exit_code = curl_exec($c);
    curl_close($c);
    $exit_array = json_decode($exit_code, null, 512, JSON_THROW_ON_ERROR);
    echo "exit array: \r\n";
    var_dump($exit_array);
    return $exit_array->total_accepted_recipients;
}
