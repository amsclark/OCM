<?php

chdir('../');
define('PL_DISABLE_SECURITY', true);
require_once('pika-danio.php');
pika_init();
require_once('app/lib/pikaActivity.php');
require_once('app/lib/pikaMisc.php');


if (!($_REQUEST['key'] === pl_settings_get('inbound_email_auth_key'))) {
    error_log('invalid api key');
    die('invalid api key');
}



//Substantially rewrote all of this to receive emails from Mailgun instead.
$friendlyFrom = pl_clean_form_input($_POST['sender']);
$friendlyRecipient = pl_clean_form_input($_POST['recipient']); // This is the email that triggered the route. It can be a bcc'ed email.
$friendlyTo = pl_clean_form_input($_POST['To']);
$friendlyCc = pl_clean_form_input($_POST['Cc']);
$friendlySubject = pl_clean_form_input($_POST['subject']);
$friendlyContent = pl_clean_form_input($_POST['body-plain']);

//error_log("got inbound email {$friendlyFrom} {$friendlyTo} {$friendlySubject} {$friendlyBody}");

$sql = "SELECT email, user_id from users where enabled = '1' and email IS NOT NULL";
$result = DB::query($sql) or trigger_error();
$users = array();
while ($row = DBResult::fetchRow($result)) {
    $emails[$row['email']] = $row['user_id'];
}



if (array_key_exists($friendlyFrom, $emails)) {
    $userId = $emails[$friendlyFrom];
} else {
    error_log('Inbound email rejected: ' . $friendlyFrom . ' is not an active user');
    die('Inbound email rejected: ' . $friendlyFrom . ' is not an active user');
}

$caseNumParts = explode('@', $friendlyRecipient);
$caseNumOnly = DB::escapeString($caseNumParts[0]);
$mailgunDomain = DB::escapeString($caseNumParts[1]);
$sql2 = "SELECT case_id FROM cases WHERE number = '" . $caseNumOnly . "'";
$result2 = DB::query($sql2) or trigger_error();
$numResults = DBResult::numRows($result2);
if ($numResults == 1) {
    $row = DBResult::fetchRow($result2);
    $caseID = $row['case_id'];
} else {
    $caseID = '';
    //do outbound email here to sender letting them know the case number was invalid
    $bounce_from = pl_settings_get('mailgun_sending_address');
    $bounce_to = $friendlyFrom;
    $bounce_subject = "Re: " . $friendlySubject;
    $bounce_body = "The case number used in the Mailgun receiving address, " . $caseNumOnly . ", is not a valid case number and your message could not be delivered to an OCM/Pika case. Please check the case number and try again.";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/' . $mailgunDomain . '/messages');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_USERPWD, "api:" . pl_settings_get('mailgun_sending_key'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, array(
      'from' => $bounce_from,
      'to' => $bounce_to,
      'subject' => $bounce_subject,
      'text' => $bounce_body
    ));
    $result = curl_exec($ch);
    curl_close($ch);
    error_log('Inbound email rejected: ' . $caseNumOnly . ' is not a valid case number');
    die('Inbound email rejected: ' . $caseNumOnly . ' is not a valid case number');
}

$email_note = new pikaActivity();
$email_note->setValue('summary', '[Incoming Email System]');
$email_notes = "From: " . $friendlyFrom . PHP_EOL;
$email_notes .= "To: " . $friendlyTo . PHP_EOL;
$email_notes .= "CC: " . $friendlyCc . PHP_EOL;
$email_notes .= "Subject: " . $friendlySubject . PHP_EOL;
$email_notes .= "--------" . PHP_EOL . $friendlyContent;
$email_note->setValue('notes', $email_notes);
$email_note->setValue('user_id', $userId);
$email_note->setValue('case_id', $caseID);
$email_note->setValue('act_date', date('Y-m-d'));
$email_note->setValue('act_time', date('H:i:s'));
//error_log($email_notes);
$email_note->save();

$attachments = array();
foreach ($_FILES as $key => $value) {
    //check for attachment keys like 'attachment-1', 'attachment-2'
    if (preg_match('/^attachment-\d+$/', $key)) {
        $attachments[] = $key;
    }
}

if (!empty($attachments)) {
    require_once('pikaDocument.php');
    foreach ($attachments as $attachmentKey) {
        // Extract the file data
        $file = $_FILES[$attachmentKey];
        //error_log(print_r($file, true));
        // Validate the file
        if ($file && $file['error'] === UPLOAD_ERR_OK) {
            $doc = new pikaDocument();
            // Prepare the file data for upload
            $x = [
                'name' => $file['name'],          // Original file name
                'type' => $file['type'],          // MIME type (e.g., application/pdf)
                'tmp_name' => $file['tmp_name'],  // Temporary file location
                'error' => $file['error'],        // Error code (0 means no error)
                'size' => $file['size']           // File size in bytes
            ];
            // Upload the document
            $doc->uploadDoc($x, $description, $parent_folder, 'C', $caseID);
        } else {
            // Log any errors with the file upload
            error_log("Failed to process attachment: $attachmentKey. Error code: " . $file['error']);
        }
    }
}
