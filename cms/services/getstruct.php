<?php

/************************************/

/* Authored 2018                    */
/* By Alex Clark at Legal Aid of    */
/* Nebraska                         */
/*                                  */
/* For questions please contact     */
/* alex@metatheria.solutions        */
/*                                  */
/* this file is free and            */
/* unencumbered software released   */
/* into the public domain under the */
/* terms of the Unlicense.          */
/*                                  */
/* https://unlicense.org            */
/************************************/
header("Content-Type: text/plain");
chdir('../');
require_once('pika-danio.php');
pika_init();


class EnumerationMenu
{
    public $menuName = null;
    public $menuItems = array();
}

class FieldDescriptorList
{
    public $tableName = null;
    public $codesAndTypes = array();
}

class DBSchema
{
    public $caseTables = array();
    public $menus = array();
}

$myDBSchema = new DBSchema();
// build the SQL statement, based on user input
$menu_prefix = "menu_%";
$params = [];
$params[] = $menu_prefix;
$sql = "show tables like ?";




// execute the SQL statement, format the results, and add to the table object
$result = DB::preparedQuery($sql, $params) or trigger_error('Could not execute query');
while (($row =  DBResult::fetchRow($result))) {
    $myEnumerated = new EnumerationMenu();
    $myEnumerated->menuName = $row[array_key_first($row)];
    $clean_table_name = DB::escapeString($row[array_key_first($row)]);
    $sql_table_list = "select * from {$clean_table_name}";
    $result_table_list = DB::preparedQuery($sql_table_list, array()) or trigger_error();
    while (($result_table_row = DBResult::fetchRow($result_table_list))) {
        array_push($myEnumerated->menuItems, $result_table_row);
    }
    array_push($myDBSchema->menus, $myEnumerated);
}

$sql2 = "show columns from contacts";
$result = DB::preparedQuery($sql2, array()) or trigger_error();
$contactsFDL = new FieldDescriptorList();
$contactsFDL->tableName = "contacts";
while (($row = DBResult::fetchRow($result))) {
    array_push($contactsFDL->codesAndTypes, $row);
}
$myDBSchema->caseTables["contacts"] = $contactsFDL;
$sql3 = "show columns from cases";
$result = DB::preparedQuery($sql3, array()) or trigger_error();
$casesFDL = new FieldDescriptorList();
$casesFDL->tableName = "cases";
while (($row = DBResult::fetchRow($result))) {
    array_push($casesFDL->codesAndTypes, $row);
}
$myDBSchema->caseTables["cases"] = $casesFDL;
$sql4 = "show columns from activities";
$result = DB::preparedQuery($sql4, array()) or trigger_error();
$activitiesFDL = new FieldDescriptorList();
$activitiesFDL->tableName = "activities";
while (($row = DBResult::fetchRow($result))) {
    array_push($activitiesFDL->codesAndTypes, $row);
}
$myDBSchema->caseTables["activities"] = $activitiesFDL;
echo json_encode($myDBSchema, JSON_PRETTY_PRINT);
exit();
