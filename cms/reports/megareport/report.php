<?php

/**********************************/

/* Pika CMS (C) 2009 Aaron Worley */
/* http://pikasoftware.com        */
/**********************************/

chdir('../../');

require_once('pika-danio.php');
pika_init();

$report_title = 'Mega Report';
$report_name = 'megareport';

$base_url = pl_settings_get('base_url');
if (!pika_report_authorize($report_name)) {
    $main_html = array();
    $main_html['base_url'] = $base_url;
    $main_html['page_title'] = $report_title;
    $main_html['nav'] = "<a href=\"{$base_url}/\">Pika Home</a>
    				  &gt; <a href=\"{$base_url}/reports/\">Reports</a> 
    				  &gt; $report_title";
    $main_html['content'] = "You are not authorized to run this report";

    $buffer = pl_template('templates/default.html', $main_html);
    pika_exit($buffer);
}

//Group A
$ffield0 = pl_grab_post('ffield0');
$ffield6 = pl_grab_post('ffield6');
$ffield7 = pl_grab_post('ffield7');

//Group B
$ffield1 = pl_grab_post('ffield1');
$ffield8 = pl_grab_post('ffield8');
$ffield9 = pl_grab_post('ffield9');

//Group C
$ffield2 = pl_grab_post('ffield2');
$ffield10 = pl_grab_post('ffield10');
$ffield11 = pl_grab_post('ffield11');

//Group D
$ffield3 = pl_grab_post('ffield3');
$ffield12 = pl_grab_post('ffield12');
$ffield13 = pl_grab_post('ffield13');

//Group E
$ffield4 = pl_grab_post('ffield4');
$ffield14 = pl_grab_post('ffield14');
$ffield15 = pl_grab_post('ffield15');


//Group F
$ffield5 = pl_grab_post('ffield5');
$ffield16 = pl_grab_post('ffield16');
$ffield17 = pl_grab_post('ffield17');


$ffield = array($ffield0, $ffield6, $ffield7,
                $ffield1, $ffield8, $ffield9,
                $ffield2, $ffield10, $ffield11,
                $ffield3, $ffield12, $ffield13,
                $ffield4, $ffield14, $ffield15,
                $ffield5, $ffield16, $ffield17);

//Group A
$fcomp0 = pl_grab_post('fcomp0');
$fcomp6 = pl_grab_post('fcomp6');
$fcomp7 = pl_grab_post('fcomp7');

//Group B
$fcomp1 = pl_grab_post('fcomp1');
$fcomp8 = pl_grab_post('fcomp8');
$fcomp9 = pl_grab_post('fcomp9');

//Group C
$fcomp2 = pl_grab_post('fcomp2');
$fcomp10 = pl_grab_post('fcomp10');
$fcomp11 = pl_grab_post('fcomp11');

//Group D
$fcomp3 = pl_grab_post('fcomp3');
$fcomp12 = pl_grab_post('fcomp12');
$fcomp13 = pl_grab_post('fcomp13');

//Group E
$fcomp4 = pl_grab_post('fcomp4');
$fcomp14 = pl_grab_post('fcomp14');
$fcomp15 = pl_grab_post('fcomp15');

//Group F
$fcomp5 = pl_grab_post('fcomp5');
$fcomp16 = pl_grab_post('fcomp16');
$fcomp17 = pl_grab_post('fcomp17');

$annotated_case_field_labels = pl_menu_get('annotate_cases');


$fcomp = array( $fcomp0, $fcomp6, $fcomp7,
                $fcomp1, $fcomp8, $fcomp9,
                $fcomp2, $fcomp10, $fcomp11,
                $fcomp3, $fcomp12, $fcomp13,
                $fcomp4, $fcomp14, $fcomp15,
                $fcomp5, $fcomp16, $fcomp17);


foreach ($fcomp as $key => $val) {
    if ('&lt;' == $val) {
        $fcomp[$key] = '<';
    } elseif ('&gt;' == $val) {
        $fcomp[$key] = '>';
    }
}

// Group A
$fvalue0 = pl_grab_post('fvalue0');
$fvalue6 = pl_grab_post('fvalue6');
$fvalue7 = pl_grab_post('fvalue7');

// Group B
$fvalue1 = pl_grab_post('fvalue1');
$fvalue8 = pl_grab_post('fvalue8');
$fvalue9 = pl_grab_post('fvalue9');

// Group C
$fvalue2 = pl_grab_post('fvalue2');
$fvalue10 = pl_grab_post('fvalue10');
$fvalue11 = pl_grab_post('fvalue11');

// Group D
$fvalue3 = pl_grab_post('fvalue3');
$fvalue12 = pl_grab_post('fvalue12');
$fvalue13 = pl_grab_post('fvalue13');

// Group E
$fvalue4 = pl_grab_post('fvalue4');
$fvalue14 = pl_grab_post('fvalue14');
$fvalue15 = pl_grab_post('fvalue15');

// Group F
$fvalue5 = pl_grab_post('fvalue5');
$fvalue16 = pl_grab_post('fvalue16');
$fvalue17 = pl_grab_post('fvalue17');


$fvalue = array($fvalue0, $fvalue6, $fvalue7,
                $fvalue1, $fvalue8, $fvalue9,
                $fvalue2, $fvalue10, $fvalue11,
                $fvalue3, $fvalue12, $fvalue13,
                $fvalue4, $fvalue14, $fvalue15,
                $fvalue5, $fvalue16, $fvalue17);


$sum = pl_grab_post('sum');
$count = pl_grab_post('count');
$order_by = pl_grab_post('order_by');
$order_by2 = pl_grab_post('order_by2');
$group_by = pl_grab_post('group_by');
$group_by2 = pl_grab_post('group_by2');
$users_list = pl_grab_post('users_list');
$recordlimit = pl_grab_post('recordlimit', 10000);
$relation_codes = pl_grab_post('relation_codes');


$fo = pl_grab_post('fo'); // Defines which columns to display, and in what order.
/*
if (strlen($relation_codes) > 0) {
    if (is_int(array_search('contacts.last_name',$fo)))
    {
        array_splice($fo, array_search('contacts.last_name',$fo) + 1,0,'conflict.relation_code');
    }
    else {
        array_push($fo, 'conflict.relation_code');
    }
}
*/
$showfields = '';  // This is basically the SELECT clause for the report query.
// to the SELECT clause, so the commas look correct.
$tables = array();  // Used to store table data type information.  Useful when determining
// which fields have dates or times that need to be mogrified.

$tables['cases'] = pl_table_fields_get('cases');
$tables['contacts'] = pl_table_fields_get('contacts');
$tables['activities'] = pl_table_fields_get('activities');
$report_format = pl_grab_post('report_format');
$show_sql = pl_grab_post('show_sql');
$recode_pba_id = pl_grab_post('recode_pba_id');
$recode_act_fields = pl_grab_post('recode_act_fields');
$recode_litc_fields_and_more = pl_grab_post('recode_litc_fields_and_more');

require_once('pikaMisc.php');
$pbAttorneyArray = pikaMisc::fetchPbAttorneyArray();
$menu_category = pl_menu_get('category');
$menu_funding = pl_menu_get('funding');
$menu_yes_no = pl_menu_get('yes_no');
$menu_case_status = pl_menu_get('case_status');
$menu_problem_2008 = pl_menu_get('problem_2008');
$menu_close_code_2008 = pl_menu_get('close_code_2008');
$menu_referred_by = pl_menu_get('referred_by');
$menu_access_line = pl_menu_get('access_line');
$menu_subunit = pl_menu_get('subunit');
$menu_litc_irs_funct = pl_menu_get('litc_irs_funct');
$menu_litc_csorcn = pl_menu_get('litc_csorcn');
$menu_gender = pl_menu_get('gender');
$menu_language = pl_menu_get('language');
$menu_ethnicity = pl_menu_get('ethnicity');
$menu_unit = pl_menu_get('unit');
$menu_users = pikaMisc::fetchStaffArray();
$annotated_case_field_labels = pl_menu_get('annotate_cases');

// get referral agencies
// Most orgs do not have the referral problem field, so commenting this out
/*
$referral_sql = "select distinct contacts.contact_id, contacts.last_name from contacts left join conflict on contacts.contact_id = conflict.contact_id where referral_problem is not null and referral_problem != '' and conflict.relation_code = '50'";
$referral_result = DB::query($referral_sql) or trigger_error("SQL: " . " Error: " . DB.error());
while ($referral_row = DBResult::fetchRow($referral_result)) {
$menu_referral_agencies[$referral_row['contact_id']] = $referral_row['last_name'];
}
*/



if ('csv' == $report_format) {
    require_once('app/lib/plCsvReportTable.php');
    require_once('app/lib/plCsvReport.php');
    $r = new plCsvReport();
} else {
    require_once('app/lib/plHtmlReportTable.php');
    require_once('app/lib/plHtmlReport.php');
    $r = new plHtmlReport();
}

// BUILD SELECT CLAUSE
// When calcuating SUMs, only display the sum field, and the group_by field (if specified)
if ($sum && $group_by && $group_by2) {
    $showfields = "$group_by, $group_by2, SUM($sum) as Sum";
} elseif ($sum && $group_by) {
    $showfields = "$group_by, SUM($sum) as Sum";
} elseif ($sum) {
    $showfields = "SUM($sum) as Sum";
} elseif ($count && $group_by && $group_by2) {
    $showfields = "$group_by, $group_by2, COUNT($count) as Total";
} elseif ($count && $group_by) {
    $showfields = "$group_by, COUNT($count) as Total";
} elseif ($count) {
    $showfields = "COUNT($count) as Total";
} else {
    if (sizeof($fo) < 1) {
        echo "<h1>Error:  you need to check off the fields you want displayed on this report</h1>\n";
        exit();
    } else {
        $z = implode(', ', $fo);
        $showfields = $z;
        // Always select case_id_deleteme if no grouping, COUNTing or SUMing is taking place
        // it's used to fill in the case_id in the case number link.
        $showfields .= ", cases.case_id AS case_id_deleteme";
    }
}

// Clean $showfields before sending it to MySQL.
$showfields = DB::escapeString($showfields);

if (substr_count($showfields, 'activities.') > 0) {
    $showfields .= ", activities.act_id AS act_id_deleteme";
    if (strlen($relation_codes) == 0) {
        $sql = "SELECT {$showfields} FROM activities
				LEFT JOIN cases ON activities.case_id = cases.case_id 
				LEFT JOIN contacts ON cases.client_id = contacts.contact_id WHERE 1";
    } else {
        $sql = "SELECT {$showfields} FROM activities 
				LEFT JOIN cases ON activities.case_id = cases.case_id 
				LEFT JOIN conflict on cases.case_id = conflict.case_id 
				LEFT JOIN contacts ON conflict.contact_id = contacts.contact_id 
				WHERE conflict.relation_code IN " . pl_process_comma_vals($relation_codes);
    }
} else {
    if (strlen($relation_codes) == 0) {
        $sql = "SELECT {$showfields} FROM cases 
				LEFT JOIN contacts ON cases.client_id = contacts.contact_id WHERE 1";
    } else {
        $sql = "SELECT {$showfields} FROM cases 
				LEFT JOIN conflict ON cases.case_id = conflict.case_id 
				LEFT JOIN contacts ON conflict.contact_id = contacts.contact_id 
				WHERE conflict.relation_code IN " . pl_process_comma_vals($relation_codes);
    }
}


// BUILD WHERE CLAUSE
$endpattern1 = " WHERE 1 AND ( ( ";
$endpattern2 = " ) AND ( ";
$i = 0;
$special_fields = array('counsel_id', 'pba_id');


foreach ($ffield as $key => $val) {
    if ($i == 0) {
        $sql .= " AND ( ( ";
    }
    if ($i == 3 or $i == 6 or $i == 9 or $i == 12 or $i == 15) {
        $sql .= " ) AND ( ";
    }
    if ($val) {
        $val = DB::escapeString($val);
        list($table_name, $field_name) = explode('.', $val);
        $field_data_type = $tables[$table_name][$field_name];

        if (!in_array($val, $special_fields)) {
            if ('is blank' == $fcomp[$i]) {
                if (substr($sql, -strlen($endpattern1)) === $endpattern1 or substr($sql, -strlen($endpattern2)) === $endpattern2) {
                    $sql .= " ($val IS NULL)";
                } else {
                    $sql .= " OR ($val IS NULL)";
                }
            } elseif ('is not blank' == $fcomp[$i]) {
                if (substr($sql, -strlen($endpattern1)) === $endpattern1 or substr($sql, -strlen($endpattern2)) === $endpattern2) {
                    $sql .= " ($val IS NOT NULL)";
                } else {
                    $sql .= " OR ($val IS NOT NULL)";
                }
            } elseif ('=' == $fcomp[$i]) {
                // use IN() comparison
                // first add quotes around each comma-separated search item
                $val_array = explode(",", $fvalue[$i]);
                $quoted_vals = '';
                $check_string = '';
                $y = 0;
                // cases.problem, cases.sp_problem and cases.outcome need to be handled differently because they themselves can contain a list of values
                if ($ffield[$i] == "cases.problem" or $ffield[$i] == "cases.sp_problem" or $ffield[$i] == "cases.outcome") {
                    foreach ($val_array as $x) {
                        $x = trim($x);
                        $x = DB::escapeString($x);
                        $check_string .= " FIND_IN_SET('{$x}', {$ffield[$i]}) OR ";
                    }
                    $check_string = rtrim($check_string, " OR ");
                    $y++;
                    if (substr($sql, -strlen($endpattern1)) === $endpattern1 or substr($sql, -strlen($endpattern2)) === $endpattern2) {
                        $sql .= " ({$check_string})";
                    } else {
                        $sql .= " OR ({$check_string})";
                    }
                } else {
                    foreach ($val_array as $x) {
                        $x = trim($x);
                        $x = DB::escapeString($x);


                        if ($field_data_type == 'date') {
                            $field_value = pl_date_mogrify($x);
                        } elseif ($field_data_type == 'time') {
                            $field_value = pl_time_mogrify($x);
                        } else {
                            $field_value = $x;
                        }

                        if ($y > 0) {
                            $quoted_vals .= ',';
                        }

                        $quoted_vals .= "'$field_value'";
                        $y++;
                    }

                    if (substr($sql, -strlen($endpattern1)) === $endpattern1 or substr($sql, -strlen($endpattern2)) === $endpattern2) {
                        $sql .= " ($val IN($quoted_vals))";
                    } else {
                        $sql .= " OR ($val IN($quoted_vals))";
                    }
                }
            } elseif ('!=' == $fcomp[$i]) {
                // use IN() comparison
                // first add quotes around each comma-separated search item
                $val_array = explode(",", $fvalue[$i]);
                $quoted_vals = '';
                $check_string = '';
                $y = 0;
                // cases.problem, cases.sp_problem and cases.outcome need to be handled differently because they themselves can contain a list of values
                if ($ffield[$i] == "cases.problem" or $ffield[$i] == "cases.sp_problem" or $ffield[$i] == "cases.outcome") {
                    foreach ($val_array as $x) {
                        $x = trim($x);
                        $x = DB::escapeString($x);
                        $check_string .= " NOT FIND_IN_SET('{$x}', {$ffield[$i]}) AND ";
                    }
                    $check_string = rtrim($check_string, " AND ");
                    $y++;
                    if (substr($sql, -strlen($endpattern1)) === $endpattern1 or substr($sql, -strlen($endpattern2)) === $endpattern2) {
                        $sql .= " ({$check_string})";
                    } else {
                        $sql .= " OR ({$check_string})";
                    }
                } else {
                    foreach ($val_array as $x) {
                        $x = trim($x);
                        $x = DB::escapeString($x);

                        if ($field_data_type == 'date') {
                            $field_value = pl_date_mogrify($x);
                        } elseif ($field_data_type == 'time') {
                            $field_value = pl_time_mogrify($x);
                        } else {
                            $field_value = $x;
                        }

                        if ($y > 0) {
                            $quoted_vals .= ',';
                        }

                        $quoted_vals .= "'$field_value'";
                        $y++;
                    }

                    if (substr($sql, -strlen($endpattern1)) === $endpattern1 or substr($sql, -strlen($endpattern2)) === $endpattern2) {
                        $sql .= " ($val NOT IN($quoted_vals) OR $val IS NULL)";
                    } else {
                        $sql .= " OR ($val NOT IN($quoted_vals) OR $val IS NULL)";
                    }
                }
            } elseif ('LIKE' == $fcomp[$i]) {
                // use LIKE comparison
                // handle '*' as a wildcard - will only work on string fields
                if ($field_data_type == 'date') {
                    $field_value = pl_date_mogrify($fvalue[$i]);
                } elseif ($field_data_type == 'time') {
                    $field_value = pl_time_mogrify($fvalue[$i]);
                } else {
                    $field_value = $fvalue[$i];
                }

                $field_value = str_replace('*', '%', $field_value);

                if (substr($sql, -strlen($endpattern1)) === $endpattern1 or substr($sql, -strlen($endpattern2)) === $endpattern2) {
                    $sql .= " ($val LIKE '$field_value')";
                } else {
                    $sql .= " OR ($val LIKE '$field_value')";
                }
            } elseif ('between' == $fcomp[$i]) {
                $val_array = explode(",", $fvalue[$i]);
                $val_array[0] = trim($val_array[0]);
                $val_array[1] = trim($val_array[1]);

                if ($field_data_type == 'date') {
                    $value_a = pl_date_mogrify($val_array[0]);
                    $value_b = pl_date_mogrify($val_array[1]);
                } elseif ($field_data_type == 'time') {
                    $value_a = pl_time_mogrify($val_array[0]);
                    $value_b = pl_time_mogrify($val_array[1]);
                } else {
                    //$field_value = $fvalue[$i];  - What is this???
                    $value_a = $val_array[0];
                    $value_b = $val_array[1];
                }

                $value_a = DB::escapeString($value_a);
                $value_b = DB::escapeString($value_b);

                if (substr($sql, -strlen($endpattern1)) === $endpattern1 or substr($sql, -strlen($endpattern2)) === $endpattern2) {
                    $sql .= " ($val >= '$value_a' AND $val <= '$value_b')";
                } else {
                    $sql .= " OR ($val >= '$value_a' AND $val <= '$value_b')";
                }
            } elseif ($fvalue[$i]) {
                $comp = $fcomp[$i];

                if ($field_data_type == 'date') {
                    $field_value = pl_date_mogrify($fvalue[$i]);
                } elseif ($field_data_type == 'time') {
                    $field_value = pl_time_mogrify($fvalue[$i]);
                } else {
                    $field_value = $fvalue[$i];
                }

                $field_value = DB::escapeString($field_value);
                $comp = DB::escapeString($comp);

                if (substr($sql, -strlen($endpattern1)) === $endpattern1 or substr($sql, -strlen($endpattern2)) === $endpattern2) {
                    $sql .= " ($val$comp'$field_value')";
                } else {
                    $sql .= " OR ($val$comp'$field_value')";
                }
            }
        }
        $i++;
    }
}
$sql .= " )) ";
$sql = str_ireplace('AND (  )', "", $sql);
$sql = str_ireplace('(  OR ', "(", $sql);

//echo "<pre>" . $sql . "</pre>";
//exit();


if (strlen($users_list) > 0) {
    $users_list = pl_process_comma_vals($users_list);
    //$users_list = substr($users_list, 0, (strlen($users_list) - 1));
    //$users_list = mysql_real_escape_string($users_list);

    if (strpos($showfields, 'activities.') === false) {
        $sql .= " AND (cases.user_id IN {$users_list} OR cases.cocounsel1 IN {$users_list} OR cases.cocounsel2 IN {$users_list})";
    } else {
        $sql .= " AND activities.user_id IN {$users_list}";
    }
}


// Build ORDER BY clause
if ($order_by) {
    $order_by = DB::escapeString($order_by);
    $sql .= " ORDER BY $order_by";

    if ($order_by2) {
        $order_by2 = DB::escapeString($order_by2);
        $sql .= ", $order_by2";
    }
}

// Build GROUP BY clause
if ($group_by) {
    $group_by = DB::escapeString($group_by);
    $sql .= " GROUP BY $group_by";

    if ($group_by2) {
        $group_by2 = DB::escapeString($group_by2);
        $sql .= ", $group_by2";
    }
}

// Build LIMIT clause
if (!$recordlimit) {
    $recordlimit = 1000;
}
$sql .= " LIMIT $recordlimit";


// Check if the pattern AND ( ( ) ) exists without any non-whitespace characters inside
if (preg_match('/AND\s*\(\s*\(\s*\)\s*\)\s*/i', $sql)) {
    $sql = preg_replace('/AND\s*\(\s*\(\s*\)\s*\)\s*/i', '', $sql);
}


$result = DB::query($sql) or trigger_error("SQL: " . $sql . " Error: " . DB::error());

$r->title = $report_title;
$r->display_row_count(true);

while ($row = DBResult::fetchRow($result)) {
    if ($recode_pba_id) {
        if (isset($row['pba_id1'])) {
            $row['pba_id1'] = preg_split("/[1234567890:-]/", (string) pl_array_lookup($row['pba_id1'], $pbAttorneyArray))[0];
        }
        if (isset($row['pba_id2'])) {
            $row['pba_id2'] = preg_split("/[1234567890:-]/", (string) pl_array_lookup($row['pba_id2'], $pbAttorneyArray))[0];
        }
        if (isset($row['pba_id3'])) {
            $row['pba_id3'] = preg_split("/[1234567890:-]/", (string) pl_array_lookup($row['pba_id3'], $pbAttorneyArray))[0];
        }
    }
    if ($recode_act_fields) {
        if (isset($row['category'])) {
            $row['category'] = pl_array_lookup($row['category'], $menu_category);
        }
        if (isset($row['funding'])) {
            $row['funding'] = pl_array_lookup($row['funding'], $menu_funding);
        }
        if (isset($row['user_id'])) {
            $row['user_id'] = pl_array_lookup($row['user_id'], $menu_users);
        }
    }
    if ($recode_litc_fields_and_more) {
        if (isset($row["user_id"])) {
            $row["user_id"] = pl_array_lookup($row["user_id"], $menu_users);
        }
        if (isset($row["unit"])) {
            $row["unit"] = pl_array_lookup($row["unit"], $menu_unit);
        }
        if (isset($row["problem"])) {
            $row["problem"] = pl_array_lookup($row["problem"], $menu_problem_2008);
        }
        if (isset($row["status"])) {
            $row["status"] = pl_array_lookup($row["status"], $menu_case_status);
        }
        if (isset($row["close_code"])) {
            $row["close_code"] = pl_array_lookup($row["close_code"], $menu_close_code_2008);
        }
        if (isset($row["funding"])) {
            $row["funding"] = pl_array_lookup($row["funding"], $menu_funding);
        }
        if (isset($row["referred_by"])) {
            $row["referred_by"] = pl_array_lookup($row["referred_by"], $menu_referred_by);
        }
        if (isset($row["referred1"])) {
            $row["referred1"] = pl_array_lookup($row["referred1"], $menu_referral_agencies);
        }
        if (isset($row["referred2"])) {
            $row["referred2"] = pl_array_lookup($row["referred2"], $menu_referral_agencies);
        }
        if (isset($row["referred3"])) {
            $row["referred3"] = pl_array_lookup($row["referred3"], $menu_referral_agencies);
        }
        if (isset($row["access_line"])) {
            $row["access_line"] = pl_array_lookup($row["access_line"], $menu_access_line);
        }
        if (isset($row["subunit"])) {
            $row["subunit"] = pl_array_lookup($row["subunit"], $menu_subunit);
        }
        if (isset($row["litc_reviewed"])) {
            $row["litc_reviewed"] = pl_array_lookup($row["litc_reviewed"], $menu_yes_no);
        }
        if (isset($row["litc_ci_1"])) {
            $row["litc_ci_1"] = pl_array_lookup($row["litc_ci_1"], $menu_yes_no);
        }
        if (isset($row["litc_ci_2"])) {
            $row["litc_ci_2"] = pl_array_lookup($row["litc_ci_2"], $menu_yes_no);
        }
        if (isset($row["litc_ci_3"])) {
            $row["litc_ci_3"] = pl_array_lookup($row["litc_ci_3"], $menu_yes_no);
        }
        if (isset($row["litc_ci_4"])) {
            $row["litc_ci_4"] = pl_array_lookup($row["litc_ci_4"], $menu_yes_no);
        }
        if (isset($row["litc_ci_5"])) {
            $row["litc_ci_5"] = pl_array_lookup($row["litc_ci_5"], $menu_yes_no);
        }
        if (isset($row["litc_ci_6"])) {
            $row["litc_ci_6"] = pl_array_lookup($row["litc_ci_6"], $menu_yes_no);
        }
        if (isset($row["litc_ci_7"])) {
            $row["litc_ci_7"] = pl_array_lookup($row["litc_ci_7"], $menu_yes_no);
        }
        if (isset($row["litc_ci_8"])) {
            $row["litc_ci_8"] = pl_array_lookup($row["litc_ci_8"], $menu_yes_no);
        }
        if (isset($row["litc_ci_9"])) {
            $row["litc_ci_9"] = pl_array_lookup($row["litc_ci_9"], $menu_yes_no);
        }
        if (isset($row["litc_ci_10"])) {
            $row["litc_ci_10"] = pl_array_lookup($row["litc_ci_10"], $menu_yes_no);
        }
        if (isset($row["litc_ci_11"])) {
            $row["litc_ci_11"] = pl_array_lookup($row["litc_ci_11"], $menu_yes_no);
        }
        if (isset($row["litc_ci_12"])) {
            $row["litc_ci_12"] = pl_array_lookup($row["litc_ci_12"], $menu_yes_no);
        }
        if (isset($row["litc_ci_13"])) {
            $row["litc_ci_13"] = pl_array_lookup($row["litc_ci_13"], $menu_yes_no);
        }
        if (isset($row["litc_ci_14"])) {
            $row["litc_ci_14"] = pl_array_lookup($row["litc_ci_14"], $menu_yes_no);
        }
        if (isset($row["litc_ci_15"])) {
            $row["litc_ci_15"] = pl_array_lookup($row["litc_ci_15"], $menu_yes_no);
        }
        if (isset($row["litc_ci_16"])) {
            $row["litc_ci_16"] = pl_array_lookup($row["litc_ci_16"], $menu_yes_no);
        }
        if (isset($row["litc_ci_17"])) {
            $row["litc_ci_17"] = pl_array_lookup($row["litc_ci_17"], $menu_yes_no);
        }
        if (isset($row["litc_ci_18"])) {
            $row["litc_ci_18"] = pl_array_lookup($row["litc_ci_18"], $menu_yes_no);
        }
        if (isset($row["litc_ci_19"])) {
            $row["litc_ci_19"] = pl_array_lookup($row["litc_ci_19"], $menu_yes_no);
        }
        if (isset($row["litc_ci_20"])) {
            $row["litc_ci_20"] = pl_array_lookup($row["litc_ci_20"], $menu_yes_no);
        }
        if (isset($row["litc_ci_21"])) {
            $row["litc_ci_21"] = pl_array_lookup($row["litc_ci_21"], $menu_yes_no);
        }
        if (isset($row["litc_ci_22"])) {
            $row["litc_ci_22"] = pl_array_lookup($row["litc_ci_22"], $menu_yes_no);
        }
        if (isset($row["litc_ci_23"])) {
            $row["litc_ci_23"] = pl_array_lookup($row["litc_ci_23"], $menu_yes_no);
        }
        if (isset($row["litc_ci_24"])) {
            $row["litc_ci_24"] = pl_array_lookup($row["litc_ci_24"], $menu_yes_no);
        }
        if (isset($row["litc_ci_25"])) {
            $row["litc_ci_25"] = pl_array_lookup($row["litc_ci_25"], $menu_yes_no);
        }
        if (isset($row["litc_ci_26"])) {
            $row["litc_ci_26"] = pl_array_lookup($row["litc_ci_26"], $menu_yes_no);
        }
        if (isset($row["litc_ci_27"])) {
            $row["litc_ci_27"] = pl_array_lookup($row["litc_ci_27"], $menu_yes_no);
        }
        if (isset($row["litc_ci_28"])) {
            $row["litc_ci_28"] = pl_array_lookup($row["litc_ci_28"], $menu_yes_no);
        }
        if (isset($row["litc_ci_29"])) {
            $row["litc_ci_29"] = pl_array_lookup($row["litc_ci_29"], $menu_yes_no);
        }
        if (isset($row["litc_ci_30"])) {
            $row["litc_ci_30"] = pl_array_lookup($row["litc_ci_30"], $menu_yes_no);
        }
        if (isset($row["litc_ci_31"])) {
            $row["litc_ci_31"] = pl_array_lookup($row["litc_ci_31"], $menu_yes_no);
        }
        if (isset($row["litc_ci_32"])) {
            $row["litc_ci_32"] = pl_array_lookup($row["litc_ci_32"], $menu_yes_no);
        }
        if (isset($row["litc_ci_33"])) {
            $row["litc_ci_33"] = pl_array_lookup($row["litc_ci_33"], $menu_yes_no);
        }
        if (isset($row["litc_ci_62"])) {
            $row["litc_ci_62"] = pl_array_lookup($row["litc_ci_62"], $menu_yes_no);
        }
        if (isset($row["litc_ci_34"])) {
            $row["litc_ci_34"] = pl_array_lookup($row["litc_ci_34"], $menu_yes_no);
        }
        if (isset($row["litc_ci_35"])) {
            $row["litc_ci_35"] = pl_array_lookup($row["litc_ci_35"], $menu_yes_no);
        }
        if (isset($row["litc_ci_36"])) {
            $row["litc_ci_36"] = pl_array_lookup($row["litc_ci_36"], $menu_yes_no);
        }
        if (isset($row["litc_ci_37"])) {
            $row["litc_ci_37"] = pl_array_lookup($row["litc_ci_37"], $menu_yes_no);
        }
        if (isset($row["litc_ci_38"])) {
            $row["litc_ci_38"] = pl_array_lookup($row["litc_ci_38"], $menu_yes_no);
        }
        if (isset($row["litc_ci_39"])) {
            $row["litc_ci_39"] = pl_array_lookup($row["litc_ci_39"], $menu_yes_no);
        }
        if (isset($row["litc_ci_40"])) {
            $row["litc_ci_40"] = pl_array_lookup($row["litc_ci_40"], $menu_yes_no);
        }
        if (isset($row["litc_ci_41"])) {
            $row["litc_ci_41"] = pl_array_lookup($row["litc_ci_41"], $menu_yes_no);
        }
        if (isset($row["litc_ci_42"])) {
            $row["litc_ci_42"] = pl_array_lookup($row["litc_ci_42"], $menu_yes_no);
        }
        if (isset($row["litc_ci_43"])) {
            $row["litc_ci_43"] = pl_array_lookup($row["litc_ci_43"], $menu_yes_no);
        }
        if (isset($row["litc_ci_44"])) {
            $row["litc_ci_44"] = pl_array_lookup($row["litc_ci_44"], $menu_yes_no);
        }
        if (isset($row["litc_ci_45"])) {
            $row["litc_ci_45"] = pl_array_lookup($row["litc_ci_45"], $menu_yes_no);
        }
        if (isset($row["litc_ci_46"])) {
            $row["litc_ci_46"] = pl_array_lookup($row["litc_ci_46"], $menu_yes_no);
        }
        if (isset($row["litc_ci_47"])) {
            $row["litc_ci_47"] = pl_array_lookup($row["litc_ci_47"], $menu_yes_no);
        }
        if (isset($row["litc_ci_48"])) {
            $row["litc_ci_48"] = pl_array_lookup($row["litc_ci_48"], $menu_yes_no);
        }
        if (isset($row["litc_ci_49"])) {
            $row["litc_ci_49"] = pl_array_lookup($row["litc_ci_49"], $menu_yes_no);
        }
        if (isset($row["litc_ci_50"])) {
            $row["litc_ci_50"] = pl_array_lookup($row["litc_ci_50"], $menu_yes_no);
        }
        if (isset($row["litc_ci_51"])) {
            $row["litc_ci_51"] = pl_array_lookup($row["litc_ci_51"], $menu_yes_no);
        }
        if (isset($row["litc_ci_52"])) {
            $row["litc_ci_52"] = pl_array_lookup($row["litc_ci_52"], $menu_yes_no);
        }
        if (isset($row["litc_ci_53"])) {
            $row["litc_ci_53"] = pl_array_lookup($row["litc_ci_53"], $menu_yes_no);
        }
        if (isset($row["litc_ci_54"])) {
            $row["litc_ci_54"] = pl_array_lookup($row["litc_ci_54"], $menu_yes_no);
        }
        if (isset($row["litc_ci_63"])) {
            $row["litc_ci_63"] = pl_array_lookup($row["litc_ci_63"], $menu_yes_no);
        }
        if (isset($row["litc_ci_55"])) {
            $row["litc_ci_55"] = pl_array_lookup($row["litc_ci_55"], $menu_yes_no);
        }
        if (isset($row["litc_ci_56"])) {
            $row["litc_ci_56"] = pl_array_lookup($row["litc_ci_56"], $menu_yes_no);
        }
        if (isset($row["litc_ci_57"])) {
            $row["litc_ci_57"] = pl_array_lookup($row["litc_ci_57"], $menu_yes_no);
        }
        if (isset($row["litc_ci_58"])) {
            $row["litc_ci_58"] = pl_array_lookup($row["litc_ci_58"], $menu_yes_no);
        }
        if (isset($row["litc_ci_59"])) {
            $row["litc_ci_59"] = pl_array_lookup($row["litc_ci_59"], $menu_yes_no);
        }
        if (isset($row["litc_ci_60"])) {
            $row["litc_ci_60"] = pl_array_lookup($row["litc_ci_60"], $menu_yes_no);
        }
        if (isset($row["litc_ci_61"])) {
            $row["litc_ci_61"] = pl_array_lookup($row["litc_ci_61"], $menu_yes_no);
        }
        if (isset($row["litc_irs_funct"])) {
            $row["litc_irs_funct"] = pl_array_lookup($row["litc_irs_funct"], $menu_litc_irs_funct);
        }
        if (isset($row["litc_irs_multiple_matters"])) {
            $row["litc_irs_multiple_matters"] = pl_array_lookup($row["litc_irs_multiple_matters"], $menu_yes_no);
        }
        if (isset($row["litc_controversy_gt50k"])) {
            $row["litc_controversy_gt50k"] = pl_array_lookup($row["litc_controversy_gt50k"], $menu_yes_no);
        }
        if (isset($row["litc_multiple_tax_yrs"])) {
            $row["litc_multiple_tax_yrs"] = pl_array_lookup($row["litc_multiple_tax_yrs"], $menu_yes_no);
        }
        if (isset($row["litc_joint_rep"])) {
            $row["litc_joint_rep"] = pl_array_lookup($row["litc_joint_rep"], $menu_yes_no);
        }
        if (isset($row["litc_rep_vol"])) {
            $row["litc_rep_vol"] = pl_array_lookup($row["litc_rep_vol"], $menu_yes_no);
        }
        if (isset($row["litc_stm_handled"])) {
            $row["litc_stm_handled"] = pl_array_lookup($row["litc_stm_handled"], $menu_yes_no);
        }
        if (isset($row["litc_taxpayer_compliance_filing"])) {
            $row["litc_taxpayer_compliance_filing"] = pl_array_lookup($row["litc_taxpayer_compliance_filing"], $menu_yes_no);
        }
        if (isset($row["litc_taxpayer_compliance_collection"])) {
            $row["litc_taxpayer_compliance_collection"] = pl_array_lookup($row["litc_taxpayer_compliance_collection"], $menu_yes_no);
        }
        if (isset($row["litc_taxpayer_esl"])) {
            $row["litc_taxpayer_esl"] = pl_array_lookup($row["litc_taxpayer_esl"], $menu_yes_no);
        }
        if (isset($row["litc_csorch"])) {
            $row["litc_csorch"] = pl_array_lookup($row["litc_csorch"], $menu_litc_csorcn);
        }
        if (isset($row["litc_taxpayer_compliance_collect"])) {
            $row["litc_taxpayer_compliance_collect"] = pl_array_lookup($row["litc_taxpayer_compliance_collect"], $menu_yes_no);
        }
        if (isset($row["language"])) {
            $row["language"] = pl_array_lookup($row["language"], $menu_language);
        }
        if (isset($row["gender"])) {
            $row["gender"] = pl_array_lookup($row["gender"], $menu_gender);
        }
        if (isset($row["ethnicity"])) {
            $row["ethnicity"] = pl_array_lookup($row["ethnicity"], $menu_ethnicity);
        }
    }

    if (isset($row['open_date'])) {
        $row['open_date'] = pl_date_unmogrify($row['open_date']);
    }

    if (isset($row['relation_code'])) {
        $row['relation_code'] = pl_array_lookup($row['relation_code'], pl_menu_get('relation_codes'));
    }

    if (isset($row['close_date'])) {
        $row['close_date'] = pl_date_unmogrify($row['close_date']);
    }

    if (isset($row['act_date'])) {
        $row['act_date'] = pl_date_unmogrify($row['act_date']);
    }

    if (isset($row['act_time'])) {
        $row['act_time'] = pl_time_unmogrify($row['act_time']);
    }

    if ($report_format != 'csv' && isset($row['number'])) {
        $url_number = urlencode($row['number']);
        $row['number'] = "<a class=\"btn btn-sm btn-outline-info\" href=\"{$base_url}/search.php?s={$url_number}\">{$row['number']}</a>";
    }

    unset($row['case_id_deleteme']);

    if ($report_format != 'csv' && isset($row['act_date'])) {
        $row['act_date'] = "<a class=\"btn btn-sm btn-outline-info\" href=\"{$base_url}/activity.php?act_id={$row['act_id_deleteme']}\">{$row['act_date']}</a>";
    }

    if ($report_format != 'csv' && isset($row['act_time'])) {
        $row['act_time'] = "<a class=\"btn btn-sm btn-outline-info\" href=\"{$base_url}/activity.php?act_id={$row['act_id_deleteme']}\">{$row['act_time']}</a>";
    }

    unset($row['act_id_deleteme']);
    $r->set_header(array_keys($row));
    $table_headers = array_keys($row);
    $r->add_row($row);
}

if ($show_sql) {
    $r->set_sql($sql);
}

// FVAP and LAON requested that the report output to have annotated labels rather than the database field names, I think it makes sense since the annotated labels are what they check off in Megareport, so I'm making this a standard part of Megareport now
$annotated_headers = array();
foreach ($table_headers as $table_header) {
    $annotated_headers[] = pl_array_lookup($table_header, $annotated_case_field_labels);
}
$r->set_header($annotated_headers);



$r->display();
exit();
