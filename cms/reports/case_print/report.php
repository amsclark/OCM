<?php

//TODO make sure access control works right. Any user should be able to access this "report" but if they don't have access to the case_id, they should get an access denied.

chdir('../../');
require_once('pika-danio.php');
pika_init();
require_once('pikaMisc.php');
require_once('pikaCase.php');

$case_id = pl_grab_var('case_id');



// display a single, case-related activity record in HTML
function case_note($contact)
{
    global $plMenus, $tmpstaff;
    $plMenus['user_id'] = $tmpstaff;

    $notes_found = false;
    $hours = "error";
    $tmpname = "error";
    $C = '';

    if (isset($contact['user_id']) && $contact['user_id']) {
        if ($plMenus['user_id'][$contact['user_id']]) {
            $tmpname = $plMenus['user_id'][$contact['user_id']];
        } else {
            $tmpname = $contact['user_id'];
        }
    } elseif (isset($contact['pba_id']) && $contact['pba_id']) {
        if (isset($plMenus['pba_id']) && $plMenus['pba_id'][$contact['pba_id']]) {
            $tmpname = $plMenus['pba_id'][$contact['pba_id']];
        } else {
            $tmpname = $contact['pba_id'];
        }
    } else {
        $tmpname = "Not Assigned!";
    }


    $C .= sprintf(
        "<i>%s, %s</i><br>\n",
        pl_date_unmogrify($contact["act_date"]),
        $tmpname
    );


    // AMW - 2012-12-21 - added conditional display of
    // last_changed_user_id information.
    if (
        $contact['user_id'] != $contact['last_changed_user_id'] &&
            $contact['last_changed_user_id'] !== null
    ) {
        $C .= '[i](Last edited by ' .
              pl_array_lookup(
                  $contact['last_changed_user_id'],
                  $plMenus['user_id']
              ) .
                    ")[/i]\n\n";
    }
    // End AMW


    if ($contact["summary"]) {
        $C .= '<tt>' . pl_html_text($contact["summary"]) . '</tt><br>';

        $notes_found = true;
    }

    if ($contact["notes"]) {
        $C .= '<tt>' . pl_html_text($contact["notes"]) . '</tt><br>';

        $notes_found = true;
    }

    if (false == $notes_found) {
        $C .= '<em>No case notes entered</em><br>';
    }

    $C .= '&nbsp;<br>';

    return $C;
}


$plMenus['sp_problem'] = pl_menu_get('sp_problem');
$plMenus['reject_code'] = pl_menu_get('reject_code');
$plMenus['case_status'] = pl_menu_get('case_status');
$plMenus['citizen'] = pl_menu_get('citizen');
$plMenus['funding'] = pl_menu_get('funding');
$plMenus['intake_type'] = pl_menu_get('intake_type');
$plMenus['income_type'] = pl_menu_get('income_type');
$plMenus['lsc_income_change'] = pl_menu_get('lsc_income_change');
$plMenus['asset_type'] = pl_menu_get('asset_type');
$plMenus['office'] = pl_menu_get('office');
$plMenus['referred_by'] = pl_menu_get('referred_by');
$plMenus['ethnicity'] = pl_menu_get('ethnicity');
$plMenus['language'] = pl_menu_get('language');
$plMenus['residence'] = pl_menu_get('residence');
$plMenus['marital'] = pl_menu_get('marital');
$plMenus['outcome'] = pl_menu_get('outcome');
$plMenus['main_benefit'] = pl_menu_get('main_benefit');
$plMenus['gender'] = pl_menu_get('gender');
$plMenus['yes_no'] = pl_menu_get('yes_no');
$plMenus['just_income'] = pl_menu_get('just_income');

$pba_array = pikaMisc::fetchPbAttorneyArray();

$tmpstaff = pikaMisc::fetchStaffArray();



$case = new pikaCase($case_id);
$a = $case->getValues();

$params = [];
$params[] = $a['client_id'];
$sql = "SELECT * from contacts WHERE contact_id= ? LIMIT 1";
$result = DB::preparedQuery($sql, $params);
$b = DBResult::fetchRow($result);

$a = array_merge($a, $b);

$current_year = date('Y');
$current_datetime = date('U');

$year_opened = substr($a['open_date'], 0, 4);
$year_closed = substr($a['close_date'], 0, 4);


// Use 2008 codes.
$plMenus['problem'] = pl_menu_get('problem_2008');
$plMenus['close_code'] = pl_menu_get('close_code_2008');





if (is_null($a['conflicts'])) {
    $a['conflict_descr'] = 'No information';
} elseif ($a['conflicts']) {
    $a['conflict_descr'] = 'One or more conflicts exist';
} else {
    $a['conflict_descr'] = 'No conflicts exist';
}



$a['primary_client_name'] = $a['last_name'];
if ($a['first_name']) {
    $a['primary_client_name'] .= ", {$a['first_name']} {$a['middle_name']} {$a['extra_name']}";
}

$a['org_name'] = $plTemplate['org_name'];
$a['username'] = $auth_row['username'];

$a['full_address'] = "{$a['address']}";
if ($a['address2']) {
    $a['full_address'] .= "<br>{$a['address2']}";
}
$a['full_address'] .= "<br>{$a['city']} {$a['state']} {$a['zip']}";

// Phone number
$a['phone_number'] = $a['phone'];
if ($a['area_code']) {
    $a['phone_number'] = "({$a['area_code']})" . $a['phone_number'];
}
if ($a['phone_notes']) {
    $a['phone_number'] .= "<br>" . $a['phone_notes'];
}

// Altername phone number
$a['phone_number_alt'] = $a['phone_alt'];
if ($a['area_code_alt']) {
    $a['phone_number_alt'] = "({$a['area_code_alt']})" . $a['phone_number_alt'];
}
if ($a['phone_notes_alt']) {
    $a['phone_number_alt'] .= "<br>" . $a['phone_notes_alt'];
}

$a['birth_date'] = pl_date_unmogrify($a['birth_date']);
$a['open_date'] = pl_date_unmogrify($a['open_date']);
$a['close_date'] = pl_date_unmogrify($a['close_date']);
$a['close_code'] = pl_array_lookup($a['close_code'], $plMenus['close_code']);
$a['problem'] = pl_array_lookup($a['problem'], $plMenus['problem']);
$a['sp_problem'] = pl_array_lookup($a['sp_problem'], $plMenus['sp_problem']);
$a['reject_code'] = pl_array_lookup($a['reject_code'], $plMenus['reject_code']);
$a['status'] = pl_array_lookup($a['status'], $plMenus['case_status']);
$a['conflicts'] = pl_array_lookup($a['conflicts'], $plMenus['yes_no']);
$a['citizen'] = pl_array_lookup($a['citizen'], $plMenus['citizen']);
$a['dom_viol'] = pl_array_lookup($a['dom_viol'], $plMenus['yes_no']);
$a['sex_assault'] = pl_array_lookup($a['sex_assault'], $plMenus['yes_no']);
$a['stalking'] = pl_array_lookup($a['stalking'], $plMenus['yes_no']);
$a['funding'] = pl_array_lookup($a['funding'], $plMenus['funding']);
$a['undup'] = pl_array_lookup($a['undup'], $plMenus['yes_no']);
$a['intake_type'] = pl_array_lookup($a['intake_type'], $plMenus['intake_type']);
$a['lsc_income_change'] = pl_array_lookup($a['lsc_income_change'], $plMenus['lsc_income_change']);
$a['office'] = pl_array_lookup($a['office'], $plMenus['office']);
$a['referred_by'] = pl_array_lookup($a['referred_by'], $plMenus['referred_by']);
$a['ethnicity'] = pl_array_lookup($a['ethnicity'], $plMenus['ethnicity']);
$a['gender'] = pl_array_lookup($a['gender'], $plMenus['gender']);
$a['residence'] = pl_array_lookup($a['residence'], $plMenus['residence']);
$a['language'] = pl_array_lookup($a['language'], $plMenus['language']);
$a['intake_user_id'] = pl_array_lookup($a['intake_user_id'], $tmpstaff);
$a['marital'] = pl_array_lookup($a['marital'], $plMenus['marital']);
$a['outcome'] = pl_array_lookup($a['outcome'], $plMenus['outcome']);
$a['main_benefit'] = pl_array_lookup($a['main_benefit'], $plMenus['main_benefit']);
$a['just_income'] = pl_array_lookup($a['just_income'], $plMenus['just_income']);

$a['probono'] = pl_array_lookup($a['pba_id1'], $pba_array);
if ($a['pba_id2']) {
    $a['probono'] .= "<br>" . pl_array_lookup($a['pba_id2'], $pba_array);
}
if ($a['pba_id3']) {
    $a['probono'] .= "<br>" . pl_array_lookup($a['pba_id3'], $pba_array);
}

/*
if ($a['conflicts'] == 1)
{
    $a['conflicts'] = 'Yes';
}
else
{
    $a['conflicts'] = 'No';
}
*/



// Paul Mundt suggests using Monthly figures, not Annual
if ($a['annual0']) {
    $a['monthly0'] = round($a['annual0'] / 12, 2);
}

if ($a['annual1']) {
    $a['monthly1'] = round($a['annual1'] / 12, 2);
}

if ($a['annual2']) {
    $a['monthly2'] = round($a['annual2'] / 12, 2);
}

if ($a['annual3']) {
    $a['monthly3'] = round($a['annual3'] / 12, 2);
}

if ($a['annual4']) {
    $a['monthly4'] = round($a['annual4'] / 12, 2);
}

if ($a['annual5']) {
    $a['monthly5'] = round($a['annual5'] / 12, 2);
}

if ($a['annual6']) {
    $a['monthly6'] = round($a['annual6'] / 12, 2);
}

if ($a['annual7']) {
    $a['monthly7'] = round($a['annual7'] / 12, 2);
}

if ($a['income']) {
    $a['monthly_income'] = round($a['income'] / 12, 2);
}

$a['income_type0'] = pl_array_lookup($a['income_type0'], $plMenus['income_type']);
$a['income_type1'] = pl_array_lookup($a['income_type1'], $plMenus['income_type']);
$a['income_type2'] = pl_array_lookup($a['income_type2'], $plMenus['income_type']);
$a['income_type3'] = pl_array_lookup($a['income_type3'], $plMenus['income_type']);
$a['income_type4'] = pl_array_lookup($a['income_type4'], $plMenus['income_type']);
$a['income_type5'] = pl_array_lookup($a['income_type5'], $plMenus['income_type']);
$a['income_type6'] = pl_array_lookup($a['income_type6'], $plMenus['income_type']);
$a['income_type7'] = pl_array_lookup($a['income_type7'], $plMenus['income_type']);


$a['asset_type0'] = pl_array_lookup($a['asset_type0'], $plMenus['asset_type']);
$a['asset_type1'] = pl_array_lookup($a['asset_type1'], $plMenus['asset_type']);
$a['asset_type2'] = pl_array_lookup($a['asset_type2'], $plMenus['asset_type']);
$a['asset_type3'] = pl_array_lookup($a['asset_type3'], $plMenus['asset_type']);
$a['asset_type4'] = pl_array_lookup($a['asset_type4'], $plMenus['asset_type']);


$x = $y = $z = 0;

$a['additionals'] = "<ul>\n";
$a['opposings'] = "<ul>\n";
$a['other_contacts'] = "<ul>\n";

// 02-16-2012 - caw - added a table of additional cases where current client was/is also a client
$a['other_cases'] = "<table><tr><td><u>Case Num</u></td><td><u>Date</u></td><td><u>Date</u></td><td><u>Code</u></td></tr>";
$other_cases_found = false;
$dummy = null;
if ($a['client_id']) {
    //$result = $pk->fetchCaseList(array('client_id' => $a['client_id']), $dummy);
    $params = [];
    $params[] = $a['client_id'];
    $sql = "SELECT 	case_id, 		
									number, 	
									problem, 		
									status, 		
									cases.user_id, 	
									cocounsel1, 
									cocounsel2, 
									office, 	
									open_date, 	
									close_date, 
									funding, 				
									client_id, 
									contacts.first_name as `contacts.first_name`, 
									contacts.middle_name AS `contacts.middle_name`,
									contacts.last_name AS `contacts.last_name`, 
									contacts.extra_name AS `contacts.extra_name`, 
									area_code, 
									phone, 
									users.first_name as `users.first_name`, 
									users.middle_name as `users.middle_name`,	
									users.last_name as `users.last_name`,
									users.extra_name as `users.extra_name` 
									FROM cases 
									LEFT JOIN contacts ON cases.client_id=contacts.contact_id 
									LEFT JOIN users on cases.user_id=users.user_id 
									WHERE cases.client_id = ?";
    $result = DB::preparedQuery($sql, $params);

    while ($row = DBResult::fetchRow($result)) {
        if ($a['case_id'] <> $row['case_id']) {
            $other_cases_found = true;
            if ($row['number']) {
                $number_temp = $row['number'];
            } else {
                $number_temp = "Case Number Missing";
            }
            if ($row['open_date']) {
                $open_temp = pl_date_unmogrify($row['open_date']);
            } else {
                $open_temp = "Unknown";
            }
            if ($row['close_date']) {
                $close_temp = pl_date_unmogrify($row['close_date']);
            } else {
                $close_temp = "Not Closed";
            }
            if ($row['problem']) {
                $problem_temp = $row['problem'];
            } else {
                $problem_temp = "n/a";
            }
            $a['other_cases'] .= "<tr><td><h5>$number_temp &nbsp</h5></td><td><h5>$open_temp </h5></td><td><h5>$close_temp &nbsp</h5></td><td><h5>$problem_temp</h5></td></tr>";
        }  // end of not current case
    }  // end of while
} // end of if client exists
if (!$other_cases_found) {
    $a['other_cases'] = "None Found";
} else {
    $a['other_cases'] .= "</table>";
}

$a['additional'] = '';
$rc = pl_menu_get('relation_codes');

$params = [];
$params[] = $case_id;
$sql = "SELECT conflict.conflict_id, conflict.relation_code, contacts.*, menu_relation_codes.label
			    FROM conflict
			    LEFT JOIN contacts
			    ON conflict.contact_id=contacts.contact_id 
				LEFT JOIN menu_relation_codes
				ON conflict.relation_code=menu_relation_codes.value
				WHERE conflict.case_id= ? ";
$result = DB::preparedQuery($sql, $params);
while ($row = DBResult::fetchRow($result)) {
    $nametmp = pl_text_name($row);
    $phonetmp = pl_text_phone($row);

    switch ($row["relation_code"]) {
        case 1:
            $relatmp = "Client";
            break;

        case 2:
            $relatmp = "Opposing Party";
            break;

        default:
            $relatmp = pl_array_lookup($row["relation_code"], $rc);
            break;
    }

    if ($row['contact_id'] != $a['client_id']) {
        $a['additional'] .= "$relatmp:  <b>$nametmp</b><br>\n";

        if ($phonetmp) {
            $a['additional'] .= "$phonetmp<br>\n";
        }

        if (strlen(trim($row['phone_notes'])) > 0) {
            $a['additional'] .= $row['phone_notes'] . "<br>\n";
        }

        if (strlen(trim($row['notes'])) > 0) {
            $a['additional'] .= $row['notes'] . "<br>\n";
        }

        $a['additional'] .= "<br>\n";
    }
}


$a['user_id'] = pl_array_lookup($a['user_id'], $tmpstaff);
$a['cocounsel1'] = pl_array_lookup($a['cocounsel1'], $tmpstaff);
$a['cocounsel2'] = pl_array_lookup($a['cocounsel2'], $tmpstaff);

$params = [];
$params[] = $case_id;
$sql = $sql = "SELECT activities.*,
								users.first_name, 
								users.last_name
					FROM activities
					LEFT JOIN users ON activities.user_id=users.user_id
					WHERE case_id= ? 
					ORDER BY act_date ASC, act_time ASC, last_changed ASC";
$result = DB::preparedQuery($sql, $params);

$i = $hours_worked = 0;

// A link to reverse the current display order of activities
// Note:  Show this only if there are cases to display
$notes_count = DBResult::numRows($result);
if (0 == $notes_count) {
    $a['case_notes'] = "No case notes exist for this case.";
} else { // only show these if there are actually activities to sort...
    $a['case_notes'] = '';
    while ($row = DBResult::fetchRow($result)) {
        $a['case_notes'] .= case_note($row);

        if ($row["completed"]) {
            $hours_worked += $row["hours"];
        }
    }

    $a['case_notes'] .= "Total of <b>$hours_worked</b> hours completed on this case";
}

if (pl_grab_var('info')) {
    $a['sub_info'] = pl_template($a, 'subtemplates/case_print_info.html');
}

if (pl_grab_var('notes')) {
    $a['sub_notes'] = pl_template($a, 'subtemplates/case_print_notes.html');
}

$buffer = pl_template($a, 'reports/case_print/case_print.html');

//$rep->display($buffer);
echo $buffer;

exit();
