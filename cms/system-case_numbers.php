<?php

/***********************************/

/* Pika CMS (C) 2002 Aaron Worley */
/* http://pikasoftware.com         */
/*                                 */
/* Modified December 2019          */
/* By Metatheria, LLC              */
/* https://metatheria.solutions    */
/***********************************/

require_once('pika-danio.php');
pika_init();

require_once('pikaMisc.php');


$a = array();
$a['base_url'] = $base_url = pl_settings_get('base_url');


// SECURITY
if (!pika_authorize('system', array())) {
    $main_html = array();
    $main_html["page_title"] = 'Case Numbering';
    $main_html['nav'] = "<a href=\"{$base_url}/\">%%[branding]%% Home</a> &gt;
						<a href=\"{$base_url}/site_map.php\">Site Map</a> &gt;
	 					Case Numbering";
    $main_html["content"] = 'Access Denied';

    $buffer = pl_template('templates/default.html', $main_html);
    pika_exit($buffer);
}

// VARIABLES

$action = pl_grab_post('action');
$case_number = pl_grab_post('case_number');


if ($action == 'update') {
    if ($case_number >= 0 && is_numeric($case_number)) {
        $safe_case_number = DB::escapeString($case_number);
        DB::query("LOCK TABLES counters WRITE") or trigger_error('counters table lock failed'); // prepared queries not supported for LOCK TABLES
        $params = [];
        $params[] = $safe_case_number;
        DB::preparedQuery("UPDATE counters SET count=? WHERE id='case_number' LIMIT 1", $params);
        DB::query("UNLOCK TABLES") or trigger_error('error'); // prepared queries not supported for UNLOCK TABLES
    }
}

$current_year = date('Y');
$params = [];
$params[] = $current_year . "-01-01";
$params[] = $current_year . "-12-31";
$sql = "SELECT COUNT(number) AS nbr
		FROM cases
		WHERE 1 
		AND open_date >= ? 
		AND open_date <= ?";
$result = DB::preparedQuery($sql, $params);
$row = DBResult::fetchRow($result);
$a['cases_ytd'] = $row['nbr'];

$sql = "SELECT count FROM counters WHERE id = 'case_number' LIMIT 1";
$result = DB::preparedQuery($sql, array());
$row = DBResult::fetchRow($result);
$a['new_number'] = $row['count'] + 1;
$a['new_number_ex'] = "X-" . date('y') . "-" . str_pad(sprintf("%s", ($row['count'] + 1)), 5, '0', STR_PAD_LEFT);
$a['case_number'] = $row['count'];



$main_html = array();
$main_html["page_title"] = 'Case Numbering';
$main_html['nav'] = "<a href=\"{$base_url}/\">%%[branding]%% Home</a> 
					&gt; <a href=\"{$base_url}/site_map.php\">Site Map</a> 
					&gt; Case Numbering";
$main_html["content"] = pl_template('subtemplates/system-case_numbers.html', $a);

// add in report list for sidebar
$reports = pikaMisc::reportList(true);
$y = "";

foreach ($reports as $z) {
    $y .= "<li>{$z}</li>\n";
}

$main_html['report_list'] = $y;

$buffer = pl_template('templates/default.html', $main_html);
pika_exit($buffer);
