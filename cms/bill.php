<?php

require_once 'app/lib/Template.php';
require_once 'app/lib/Grant.php';

// this sets the server time zone automatically
require_once('pika_cms.php');

// Authentication
require_once('pika-danio.php');
pika_init();

require_once('pikaMisc.php');
require_once('pikaActivity.php');

$user_id = $auth_row['user_id'];

$grant = new Grant();

$grant_id = isset($_GET['id']) ? $_GET['id'] : null;


if (pl_grab_get('error')) {
    $error = pl_grab_get('error');

    switch ($error) {
        case 'emptyfields':
            $message = "Fill out all required fields";
            break;
        case 'caserestricted':
            $message = "Grant is case restricted";
            break;
        case 'inactivegrant':
            $message = "Cannot bill to an inactive grant";
            break;
        case 'invaliddatestart':
            $message = "Date of service precedes grant start date";
            break;
        case 'invaliddateend':
            $message = "Date of service postdates grant end date";
            break;
        case 'invalidcase':
            $message = "Case does not fit funding criteria";
            break;
        case 'invalidhours':
            $message = "Not enough hours left in grant to bill";
            // no break
        default:
            $message = "Error";
    }

    $alert = '<div class="alert alert-warning" role="alert">' .
                $message
            . '</div>';
}

if (pl_grab_post('bill-submit')) {
    $data = array();

    $data['case_id'] = pl_grab_post('case_id');
    $data['caseworker_id'] = pl_grab_post('caseworker_id');
    $data['date_of_service'] = pl_grab_post('date_of_service', 'date');
    $data['time_spent'] = pl_grab_post('hours');
    $data['menu_category'] = pl_grab_post('menu_category');

    // Checking for empty fields
    if (!$data['caseworker_id'] || !$data['date_of_service'] || !$data['time_spent'] || $data['menu_category'] == -1) {
        header('Location: bill.php?id=' . $grant_id . '&error=emptyfields');
        exit;
    }

    // to-do: check if grant is case restricted
    if (pl_grab_post('case_restricted') && !$data['case_id']) {
        header('Location: bill.php?id=' . $grant_id . '&error=caserestricted');
        exit;
    }

    // abort if user tries to bill to an inactive grant
    if (!pl_grab_post('is_active')) {
        header('Location: bill.php?id=' . $grant_id . '&error=inactivegrant');
        $alert = '<div class="alert alert-danger" role="alert">Cannot bill to an inactive grant</div>';
        exit;
    }

    // abort if user tries to bill to an expired grant
    // grab date(s) from grant
    $start_date = pl_grab_post('start_date');
    $end_date = pl_grab_post('end_date');

    //$date = date('Y-m-d', time());

    // compare dates
    // if date of service is before start date, abort
    if ($data['date_of_service'] < $start_date) {
        header('Location: bill.php?id=' . $grant_id  . '&error=invaliddatestart');
        exit;
    }

    // if date of service is after end date, abort
    if ($end_date && $end_date != '0000-00-00' && $data['date_of_service'] > $end_date) {
        header('Location: bill.php?id=' . $grant_id . '&error=invaliddateend');
        exit;
    }

    // Check if case can bill to grant
    $valid_grants = $grant->getValidGrants($data['case_id']);

    $in_array = 0;

    foreach ($valid_grants as $valid_grant) {
        if ($grant_id == $valid_grant['grant_id']) {
            $in_array = 1;
            break;
        }
    }

    if (!$in_array) {
        header('Location: bill.php?id=' . $grant_id . '&error=invalidcase');
        exit;
    }

    //$data['amount'] = pl_grab_post('unit_rate') * $data['time_spent'];
    $running_hours = pl_grab_post('running_hours');

    // time billed cannot exceed running total of hours
    if ($data['time_spent'] > $running_hours) {
        header('Location: bill.php?id=' . $grant_id . '&error=invalidhours');
        exit;
    }

    $data['new_hours'] = $running_hours - $data['time_spent'];
    $data['notes'] = pl_grab_post('notes');
    $funding_code = pl_grab_post('funding_code');

    $category = DB::escapeString($data['menu_category']);
    $params = [];
    $params[] = $category;
    $sql = "SELECT * FROM menu_category WHERE menu_order = ?";
    $result = DB::preparedQuery($sql, $params);
    $menu_category = DBResult::fetchRow($result);

    // add to activities .... hope this works
    $activity = new pikaActivity(null);
    $activity->setValue('act_date', $data['date_of_service']);
    $activity->setValue('hours', $data['time_spent']);
    $activity->setValue('category', $menu_category['value']);
    $activity->setValue('case_id', $case_id);
    $activity->setValue('user_id', $user_id);
    $activity->setValue('pba_id', $funding_code);
    $activity->setValue('notes', $data['notes']);
    $act_saved = $activity->save();

    $grant->bill($user_id, $grant_id, $data);
    $alert = '<div class="alert alert-success" role="alert">Successfully billed to grant</div>';
}

$template = new Template('subtemplates/grant-bill.php');

$template->base_url = pl_settings_get('base_url');
$template->branding = pl_settings_get('branding');
$template->owner_name = pl_settings_get('owner_name');
$template->grant = $grant->getSingleGrant($grant_id);
$template->cases = $grant->getAllCases();
$template->users = $grant->getAllUsers();
$template->categories = $grant->getMenu('menu_category');

$template->reports = pikaMisc::reportList(true);

$template->alert = $alert;

echo $template;
