<?php

/***********************************/

/* Pika CMS (C) 2002 Aaron Worley  */
/* http://pikasoftware.com         */
/*                                 */
/* Modified December 2019          */
/* By Metatheria, LLC              */
/* https://metatheria.solutions    */
/***********************************/

/*
This code adds/updates zipcodes in the zip_codes table in Pika.
*/
chdir("..");
require_once('pika-danio.php');
pika_init();


// Variables
// probably should be an array at this point
$base_url = pl_settings_get('base_url');
$screen = pl_grab_post('screen_name');
$zipcode = pl_grab_post('zipcode');
$safe_zipcode = DB::escapeString($zipcode);
$state = pl_grab_post('state');
$safe_state = DB::escapeString($state);
$city = pl_grab_post('city');
$safe_city = DB::escapeString($city);
$county = pl_grab_post('county');
$safe_county = DB::escapeString($county);
$areacode = pl_grab_post('area_code');
$safe_areacode = DB::escapeString($areacode);


switch ($screen) {
    case 'edit':
        $params = [];
        $params[] = $safe_zipcode;
        $sql = "DELETE FROM zip_codes WHERE zip= ? ";
        DB::preparedQuery($sql, $params) or trigger_error("");
        $params = [];
        $params[] = $safe_city;
        $params[] = $safe_state;
        $params[] = $safe_zipcode;
        $params[] = $safe_areacode;
        $params[] = $safe_county;
        $sql = "INSERT INTO zip_codes VALUES (?, ?, ?, ?, ?)";
        DB::preparedQuery($sql, $params) or trigger_error("");

        header("Location: {$base_url}/zipcode.php?screen_msg=Zip Code {$safe_zipcode} Successfully Updated");

        break;

    case 'add':
        $params = [];
        $params[] = $safe_zipcode;
        $sql = "DELETE FROM zip_codes WHERE zip=?";
        DB::preparedQuery($sql, $params) or trigger_error("");
        $params = [];
        $params[] = $safe_city;
        $params[] = $safe_state;
        $params[] = $safe_zipcode;
        $params[] = $safe_areacode;
        $params[] = $safe_county;
        $sql = "INSERT INTO zip_codes VALUES (?, ?, ?, ?, ?)";
        DB::preparedQuery($sql, $params) or trigger_error("");

        header("Location: {$base_url}/zipcode.php?screen_msg=Zip Code {$safe_zipcode} Successfully Added");

        break;

    default:
        trigger_error("Unknown screen code");
        die();

        break;
}

exit;
