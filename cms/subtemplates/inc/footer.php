</div>
  <footer> 
    <p style="margin: 0px"><span class="text-muted">Portions of this software are &copy; 2015 Pika Software, LLC </span></p>
  </footer>
</div>
</div>



<!--
  <footer> 
    <p style="margin: 0px"><span class="text-muted">Portions of this software are &copy; 2015 Pika Software, LLC </span></p>
  </footer>
-->
    <script>
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
        });
        $('option').each(function() {
          if (!(this.selected))
            if (this.text.substring(0,3) == "DNU")
              $(this).remove();
        });
    </script>
</body>

<!-- Modal -->
<div id="delete" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form style="display:inline;" method="post" action="grant.php">
            <input type="hidden" name="del_id" value="<?= $grant['grant_id']; ?>">
            <input type="submit" class="btn btn-danger btn-block" value="Confirm delete?">
        </form>
      </div>
    </div>

  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</html>
