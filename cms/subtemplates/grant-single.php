<?php include 'inc/header.php'; ?>
<h3 class="page-header">Grant Details <small class="text-muted"><?= $grant['source']; ?></small>

<div class="float-right">

<div class="btn-group dropleft">
    <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Action Menu
    </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="restrict-grant.php?id=<?= $grant['grant_id']; ?>">Add Restrictions</a>
            <a class="dropdown-item" href="deposit.php?id=<?= $grant['grant_id']; ?>">Deposit Funds</a>
            <a class="dropdown-item" href="bill.php?id=<?= $grant['grant_id']; ?>">Bill to Grant</a>
            <a class="dropdown-item" href="transactions.php?id=<?= $grant['grant_id']; ?>">View Fund Activity</a>
            <a class="dropdown-item" href="edit-grant.php?id=<?= $grant['grant_id']; ?>">Edit Grant</a>
        </div>
    </div>
</div>
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete">Delete</button>

</div>
</h3>

<br>

<ul class="list-group">
    <li class="list-group-item"><strong>Name of Grant (Source):</strong> <?= $grant['source']; ?></li>
    <li class="list-group-item"><strong>Funding Code:</strong> <?= $grant['funding_code']; ?></li>
    <li class="list-group-item"><strong>Description:</strong> <?= empty($grant['description']) ? 'N/A' : $grant['description']; ?></li>
    <li class="list-group-item"><strong>Billable Hours:</strong> <?= $grant['hours']; ?></li>
    <li class="list-group-item"><strong>Start Date:</strong> <?= $grant['start_date']; ?></li>
    <li class="list-group-item"><strong>End Date:</strong> <?= $grant['end_date'] && $grant['end_date'] != '0000-00-00' ? $grant['end_date'] : 'N/A'; ?></li>
    <li class="list-group-item"><strong>Active:</strong> <?= $grant['is_active'] ? 'Yes' : 'No'; ?></li>
    <li class="list-group-item"><strong>Office:</strong> <?= $grant['office_name']; ?></li>
</ul>

<div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Restrictions
          
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <ul class="list-group">
            <li class="list-group-item"><strong>Gender Restriction:</strong> <?= $grant['gender'] ?? 'N/A'; ?></li>
            <li class="list-group-item"><strong>Problem:</strong> <?= $grant['problem'] ?? 'N/A'; ?></li>
            <li class="list-group-item"><strong>SP Problem:</strong> <?= $grant['sp_problem'] ?? 'N/A'; ?></li>
            <li class="list-group-item"><strong>County Restriction:</strong> <?= $grant['county_restriction'] ? $grant['grant_county'] : 'N/A'; ?></li>
            <li class="list-group-item"><strong>Ethnicity:</strong> <?= $grant['ethnicity'] ?? 'N/A'; ?></li>
            <li class="list-group-item"><strong>Veteran:</strong> <?= $grant['veteran_grant'] == null ? 'N/A' : ($grant['veteran_grant'] ? 'Yes' : 'No'); ?></li>
            <li class="list-group-item"><strong>Case Restricted:</strong> <?= $grant['case_restricted'] == null ? 'N/A' : ($grant['case_restricted'] ? 'Yes' : 'No'); ?></li>
            <li class="list-group-item"><strong>Poverty Restricted ( Greater than or equal to %):</strong> <?= $grant['poverty_restricted'] ?? 'N/A'; ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<?php include 'inc/footer.php'; ?>