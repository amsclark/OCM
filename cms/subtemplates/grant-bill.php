<?php include 'inc/header.php'; ?>

<h3 class="page-header">Bill to Grant <small class="text-muted"><?= $grant['source']; ?></small></h3>
<br>
<form method="POST" action="bill.php?id=<?= $grant['grant_id']; ?>">
    <div class="form-group">
        <label>Case*</label>
        <select class="form-control" name="case_id">
            <option value="0">Select Case</option>
            <?php foreach ($cases as $case) : ?>
                <option value="<?= $case['case_id']; ?>"><?= $case['number']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Caseworker*</label>
        <select class="form-control" name="caseworker_id">
            <option value="0">Select Caseworker</option>
            <?php foreach ($users as $user) : ?>
                <option value="<?= $user['user_id']; ?>"><?= $user['last_name']; ?>, <?= $user['first_name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Work Activity</label>
        <select class="form-control" name="menu_category">
            <option value="-1">Select Type of Activity</option>
            <?php foreach ($categories as $category) : ?>
                <option value="<?= $category['menu_order']; ?>"><?= $category['label'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Date of Service*</label>
        <input type="date" class="form-control" name="date_of_service">
    </div>
    <div class="form-group">
        <label>Hours Spent*</label>
        <input type="number" step="0.01" class="form-control" name="hours">
        <input type="hidden" name="running_hours" value="<?= $grant['hours']; ?>">
        <input type="hidden" name="case_restricted" value="<?= $grant['case_restricted']; ?>">
        <input type="hidden" name="is_active" value="<?= $grant['is_active']; ?>">
        <input type="hidden" name="start_date" value="<?= $grant['start_date']; ?>">
        <input type="hidden" name="end_date" value="<?= $grant['end_date']; ?>">
        <input type="hidden" name="funding_code" value="<?= $grant['funding_code']; ?>">
    </div>
    <div class="form-group>">
        <label>Notes</label>
        <textarea class="form-control" name="notes"></textarea>
    </div>
    <br>
    <input type="submit" class="btn btn-success" value="Submit" name="bill-submit">
</form>

<?php include 'inc/footer.php'; ?>