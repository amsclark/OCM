<?php include 'inc/header.php'; ?>

<h3 class="page-header">Create New Grant</h3>
<br>
<form name="grantCreateForm" method="POST" action="create-grant.php" onsubmit="return validateForm()" required>
    <div class="form-group">
        <label>Office*</label>
        <select class="form-control" name="office_id">
            <?php foreach ($offices as $office) : ?>
                <option value="<?= $office['menu_order']; ?>"><?= $office['label']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Code*</label>
        <input type="text" class="form-control" name="funding_code">
    </div>
    <div class="form-group">
        <label>Source*</label>
        <input type="text" class="form-control" name="source">
    </div>
    <div class="form-group>">
        <label>Description</label>
        <textarea class="form-control" name="description"></textarea>
    </div>
    <br>
    <div class="form-group">
        <label>Start Date*</label>
        <input type="date" class="form-control" name="start_date">
    </div>
    <div class="form-group">
        <label>End Date</label>
        <input type="date" class="form-control" name="end_date">
    </div>
    <div class="form-group">
        <label>Active*</label>
        <input type="radio" id="active" name="is_active" value=1>
        <label for="active">Yes</label>
        <input type="radio" id="inactive" name="is_active" value=0>
        <label for="inactive">No</label>
    </div>

    <input type="submit" class="btn btn-success" value="Create" name="grant-submit">

</form>


<script>

function validateForm() {
  var x = document.forms["grantCreateForm"]["office_id"].value;
  if (x == "") {
    alert("Office must be selected");
    return false;
  }
  var x = document.forms["grantCreateForm"]["funding_code"].value;
  if (x == "") {
    alert("Funding Code must be filled out");
    return false;
  }
  var x = document.forms["grantCreateForm"]["source"].value;
  if (x == "") {
    alert("Source must be filled out");
    return false;
  }
  var x = document.forms["grantCreateForm"]["start_date"].value;
  if (x == "") {
    alert("A starting date must be selected");
    return false;
  }
  var x = document.forms["grantCreateForm"]["is_active"].value;
  if (x == "") {
    alert("The grant's ACTIVE STATUS must be set to either yes or no");
    return false;
  }
}

</script>

<?php include 'inc/footer.php'; ?>