<?php include 'inc/header.php'; ?>

<h3>Grant Management</h3>

<a class="btn btn-outline-primary" href="create-grant.php">Create New Grant</a>
<br><br>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Source</th>
            <th scope="col">Code</th>
            <th scope="col">Active</th>
            <th scope="col">Funds Available</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($grants as $grant) :?>
                <tr>
                    <td><a href="grant.php?id=<?= $grant['grant_id']; ?>" class="text-primary"><u><?= $grant['source']; ?></u></a></td>
                    <td><?= $grant['funding_code']; ?></td>
                    <td><?= $grant['is_active'] ? 'Yes' : 'No'; ?></td>
                    <td><u><a href="transactions.php?id=<?= $grant['grant_id']; ?>" class="text-primary"><?= $grant['hours']; ?> hrs</u></td>
                    <td><a class="btn btn-outline-secondary btn-sm btn-block" href="restrict-grant.php?id=<?= $grant['grant_id']; ?>">Add Restriction</a></td>
                    <td><a class="btn btn-outline-success btn-sm btn-block" href="edit-grant.php?id=<?= $grant['grant_id']; ?>">Edit</a></td>
                </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php include 'inc/footer.php'; ?>