<?php

/**********************************
 *
 *
 * this is the front controller
 * for GrantController.php
 *
 *
 **********************************/

chdir('../');

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once 'ops/GrantController.php';
require_once 'ops/BillController.php';

// Pika Authentication
require_once('pika-danio.php');
pika_init();

//$user_id = $auth_row['user_id'];

$grant_id = isset($_GET['id']) ? $_GET['id'] : null;
$verb = $_SERVER['REQUEST_METHOD'];

$action = isset($_GET['action']) ? $_GET['action'] : null;
$thing = isset($_GET['thing']) ? $_GET['thing'] : null;

if ($action == 'bill') {
    $bill = new BillController($verb, $grant_id, $auth_row);
    $bill->process();
} elseif ($thing == 'flags') {
    //$flag = new FlagController($verb, $grant_id, $auth_row);
    //$flag->process();
} elseif ($thing == 'transactions') {
    //$transactions = new TransactionController($verb, $grant_id, $auth_row);
    //$transactions->process();
} else {
    $controller = new GrantController($verb, $grant_id, $auth_row);
    $controller->process();
}
