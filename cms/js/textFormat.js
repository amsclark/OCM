function no_op()
{
    return;
}

function AddTag(tag)
{
    var cursorPos = $('#notes').prop('selectionStart');
    var v = $('#notes').val();
    var textBefore = v.substring(0, cursorPos);
    var textAfter = v.substring(cursorPos, v.length);
    $('#notes').val(textBefore + tag + textAfter);

}

function toggleBox(szDivID, iState) // 1 visible, 0 hidden
{
    var obj = document.layers ? document.layers[szDivID] :
    document.getElementById ?  document.getElementById(szDivID).style :
    document.all[szDivID].style;
    obj.visibility = document.layers ? (iState ? "show" : "hide") :
    (iState ? "visible" : "hidden");

}