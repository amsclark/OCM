<?php

require_once 'app/lib/Template.php';
require_once 'app/lib/Grant.php';

require_once('pika-danio.php');
pika_init();

require_once('pikaMisc.php');

$user_id = $auth_row['user_id'];

$grant_id = isset($_GET['id']) ? $_GET['id'] : null;

$grant = new Grant();

if (pl_grab_post('edit-submit')) {
    $data = array();

    $data['office_id'] = pl_grab_post('office_id');
    $data['source'] = pl_grab_post('source');
    $data['funding_code'] = pl_grab_post('funding_code');
    $data['description'] = pl_grab_post('description');
    $data['is_active'] = pl_grab_post('is_active');
    $data['start_date'] = pl_grab_post('start_date', 'date');
    $data['end_date'] = pl_grab_post('end_date', 'date');

    $grant->editGrant($grant_id, $data);
    $alert = '<div class="alert alert-success" role="alert">
                Grant updated
             </div>';
}

$template = new Template('subtemplates/grant-edit.php');

$template->grant = $grant->getSingleGrant($grant_id);
$template->base_url = pl_settings_get('base_url');
$template->branding = pl_settings_get('branding');
$template->owner_name = pl_settings_get('owner_name');
$template->reports = pikaMisc::reportList(true);
$template->offices = $grant->getAllOffices();
$template->alert = $alert;

echo $template;
