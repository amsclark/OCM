<?php

/**********************************
 *
 *
 *
 *
 *
 **********************************/

require_once 'app/lib/Template.php';
require_once 'app/lib/Grant.php';

require_once('pika-danio.php');
pika_init();

require_once('pikaMisc.php');

$user_id = $auth_row['user_id'];

$grant = new Grant();

if (isset($_POST['del_id'])) {
    $del_id = $_POST['del_id'];
    if ($grant->deleteGrant($del_id)) {
        echo 'good';
        header('Location: grants.php');
    } else {
        echo 'oh fuck oh shit';
    }
}

$template = new Template('subtemplates/grant-single.php');

//$grant_id = isset($_GET['id']) ? $_GET['id'] : null;
$grant_id = pl_grab_get('id');

$template->grant = $grant->getSingleGrant($grant_id);
$template->base_url = pl_settings_get('base_url');
$template->branding = pl_settings_get('branding');
$template->owner_name = pl_settings_get('owner_name');
$template->reports = pikaMisc::reportList(true);

echo $template;
