<?php

/***********************************/

/* Pika CMS (C) 2011 Pika Software */
/* http://pikasoftware.com         */
/*                                 */
/* Modified January 2020           */
/* By Metatheria, LLC              */
/* https://metatheria.solutions    */
/***********************************/


// GLOBAL VARIABLES
$auth_row = array();

// CONSTANTS
if (!defined('PIKA_VERSION')) {
    define('PIKA_VERSION', '8');
}
if (!defined('PIKA_REVISION')) {
    define('PIKA_REVISION', '1');
}
if (!defined('PIKA_PATCH_LEVEL')) {
    define('PIKA_PATCH_LEVEL', '0');
}
if (!defined('PIKA_CODE_NAME')) {
    define('PIKA_CODE_NAME', 'Aaptosyax');
}


/**
 * Determine whether the user identified by $row has permission to perform action $op.
 * @return boolean
 * @param string $op
 * @param array $row
*/
function pika_authorize($op, $row)
{
    global $auth_row;
    if ('system' == $auth_row['group_id']) {
        //error_log("Authorization to {$row['number']} granted. {$auth_row['username']} if in the system group\r\n");
        return true;
    }

    $allow_this = false;

    switch ($op) {
        case 'read_case':
            if ($auth_row['read_all']) {
                $allow_this = true;
                //echo "user is in security group with read_all permission";
                // error_log("Authorization to {$row['number']} granted. {$auth_row['username']} is in security group with read_all permission");
            } elseif ($row['user_id'] == $auth_row['user_id']) {
                $allow_this = true;
                // error_log("Authorization to {$row['number']} granted. {$auth_row['username']} is the primary case handler");

                //echo "case primary handler is logged in user";
            } elseif ($row['cocounsel1'] == $auth_row['user_id']) {
                $allow_this = true;
                //error_log("Authorization to {$row['number']} granted. {$auth_row['username']} is the secondary case handler");

                //echo "case secondary handler is logged in user";
            } elseif ($row['cocounsel2'] == $auth_row['user_id']) {
                $allow_this = true;
                //error_log("Authorization to {$row['number']} granted. {$auth_row['username']} is the third case handler");

                //echo "third case handler is logged in user";
            } elseif (is_null($row['user_id'])) {
                $allow_this = true;
                //error_log("Authorization to {$row['number']} granted for {$auth_row['username']} because case has no primary case handler");

                //echo "case has no primary case handler set";
            } elseif (is_null($row['office'])) {
                $allow_this = true;
                //error_log("Authorization to {$row['number']} granted for {$auth_row['username']} because office field is null");

                //echo "office is null";
            } elseif (in_array($row['office'], $auth_row['read_office'])) {
                $allow_this = true;
                //error_log("Authorization to {$row['number']} granted for {$auth_row['username']}  because case's office is in list of offices user's security group can read");
                //echo "case's office is in list of offices logged in user's security group can read";
            }
            // this is handy for intake staff who don't have a default office set

            break;


        case 'edit_case':
            if ($auth_row['edit_all']) {
                //error_log("Write access to {$row['number']} granted for {$auth_row['username']}  because user security group has the edit_all attribute");
                $allow_this = true;
            } elseif ($row['user_id'] == $auth_row['user_id']) {
                //error_log("Write access to {$row['number']} granted. {$auth_row['username']} is the primary case handler");
                $allow_this = true;
            } elseif ($row['cocounsel1'] == $auth_row['user_id']) {
                //error_log("Write access to {$row['number']} granted. {$auth_row['username']} is the second case handler");
                $allow_this = true;
            } elseif ($row['cocounsel2'] == $auth_row['user_id']) {
                //error_log("Write access to {$row['number']} granted. {$auth_row['username']} is the third casehandler");
                $allow_this = true;
            } elseif (is_null($row['user_id'])) {
                //error_log("Write access to {$row['number']} granted for {$auth_row['username']}. Case has no primary case number.");
                $allow_this = true;
            } elseif (is_null($row['office'])) {
                //error_log("Write access to {$row['number']} granted for {$auth_row['username']}. Case has no office set.");
                $allow_this = true;
            } elseif (in_array($row['office'], $auth_row['edit_office'])) {
                //error_log("Write access to {$row['number']} granted. {$auth_row['username']} is in security group with read permissions to the case's office.");
                $allow_this = true;
            }


            // this is handy for intake staff who don't have a default office set
            break;


        case 'read_act':
            if ($auth_row['read_all']) {
                $allow_this = true;
            } elseif ($row['user_id'] == $auth_row['user_id']) {
                $allow_this = true;
            }

            /* AMW - Allow anyone to read that no user owns (should only be PB). */ elseif (strlen($row['user_id']) == 0) {
                $allow_this = true;
            }

            break;

        case 'edit_doc':
            if ($auth_row['edit_all']) {
                $allow_this = true;
            } elseif ($row['user_id'] == $auth_row['user_id']) {
                $allow_this = true;
            }

            break;

        case 'edit_act':
            if ($auth_row['edit_all'] && pl_settings_get('db_name') != 'legalaidnebraska') {
                $allow_this = true;
            } elseif ($row['user_id'] == $auth_row['user_id']) {
                $allow_this = true;
            }

            /* AMW - Allow anyone to edit that no user owns (should only be PB). */ elseif (strlen($row['user_id']) == 0) {
                $allow_this = true;
            }

            break;

        case 'users':
            if ($auth_row['users']) {
                $allow_this = true;
            }

            break;

        case 'motd':
            if ($auth_row['motd']) {
                $allow_this = true;
            }

            break;

        case 'system':
        case 'delete_case':
        case 'delete_act':
            if ('system' == $auth_row['group_id']) {
                $allow_this = true;
            }

            break;
    }

    return $allow_this;
}


/**
 * Implements security on Pika reports
 * @param string $report_name - name of report requested
 * @return boolean (true/false) - true if authorized - false if otherwise
*/
function pika_report_authorize($report_name)
{

    global $auth_row;
    $allow_this = false;

    if ('system' == $auth_row['group_id']) {
        return true;
    }


    $reports = array();
    if (strlen($auth_row['reports']) > 1) {
        if (strpos($auth_row['reports'], ',') !== false) {
            $reports = explode(',', $auth_row['reports']);
        } else {
            $reports[] = $auth_row['reports'];
        }
    }
    foreach ($reports as $report) {
        if ($report_name == $report) {
            $allow_this = true;
        }
    }

    return $allow_this;
}

/**
 * Performs shutdown tasks for "danio" scripts.
 * This function should be called at the end of every "danio"-based
 * script.
 *
 * @return boolean
*/
function pika_exit($buffer)
{
    require_once('app/lib/pikaAuth.php');
    $auth_row = pikaAuth::getInstance()->getAuthRow();

    $username = '';
    if (isset($auth_row['username']) && strlen($auth_row['username']) > 0) {
        $username = $auth_row['username'];
    }

    $buffer = str_replace("<!-- username -->", pl_clean_html($username), $buffer);
    $buffer = str_replace("<!-- org_name -->", pl_settings_get('owner_name'), $buffer);

    //mysql_close();  Don't do this; it will mess up plBase autosaving.

    // BENCHMARKING
    if (pl_settings_get('enable_benchmark')) {
        $buffer .= "<p>\n";
        $buffer .= "File Size:  " . round(strlen($buffer) / 1024) . "KB<br/>\n";
        $buffer .= "Server Time:  " . pl_benchmark() . " seconds<br/>\n";
        // Transmit current buffer.
        echo $buffer;
        // Reset buffer, so the page isn't sent twice.
        $buffer = "";
        // Run pl_benchmark() again to see how long the buffer transmit took.
        $buffer .= "Transmit Time:  " . pl_benchmark() . " seconds *<br/>\n";
        $buffer .= "</p>\n";
    }


    echo $buffer;
    exit();
}


/**
 * Returns 9 if the database stores all 9 digits and two hyphens in SSN columns.
 * Returns 4 if only truncated SSNs are stored.
 * Returns 0 if SSNs are not stored.
 *
 * @return boolean
*/
function pika_ssn_mode()
{
    $result = DB::query("DESCRIBE contacts") or trigger_error(DB::error()); // prepared queries not supported for DESCRIBE

    while ($row = DBResult::fetchRow($result)) {
        if ($row['Field'] == 'ssn') {
            if ($row['Type'] == 'varchar(11)') {
                return 9;
            } elseif ($row['Type'] == 'char(4)') {
                return 4;
            } elseif ($row['Type'] == 'char(0)') {
                return 0;
            } else {
                return null;
            }
        }
    }
}


/**
 * Initializes the Pika CMS "danio" framework.
 * This function should be called at the beginning of every "danio"-based
 * script.  If the user is not authenticated, it will display the login
 * screen and exit, so the remainder of the script cannot be accessed.
 *
 * @return boolean
*/
function pika_init()
{
    global $auth_row;
    /* 2013-08-14 AMW - Copying old code into the Extensions folder often
    ends up with pika_init() getting called twice.  To make things simple
    to migrate code to Extensions, keep track of how many times pika_init
    gets called, and only let it run once. */
    static $z = 0;
    $z++;

    if ($z > 1) {
        return true;
    }

    /* Play some games with the PHP include_path so the 'gila'
    framework libraries are not available.
    */
    $include_str = './app/lib' . PATH_SEPARATOR . './app/extralib'
        . PATH_SEPARATOR . ini_get('include_path');
    ini_set('include_path', $include_str);

    // Now that the include_path is set, load the danio pl.php library.
    //TODO fix this
    require_once('pl.php');

    // Before we go any further, start the benchmark timer.
    pl_benchmark();
    // Notify PHP to use the custom Pika error handler.
    set_error_handler("pl_error_handler");

    /* Override the default PHP session handler.*/
    session_set_save_handler("pl_session_open", "pl_session_close", "pl_session_read", "pl_session_write", "pl_session_destroy", "pl_session_gc");


    // destroy all MAGIC QUOTES
    if (PHP_VERSION_ID < 80000) {
        if (get_magic_quotes_runtime() == true) {
            set_magic_quotes_runtime(false);
        }
    }
    /* The default pl_template tag prefix and suffix are '[[' and ']]',
    change this.
    */
    define('PL_TEMPLATE_PREFIX', '%%[');
    define('PL_TEMPLATE_SUFFIX', ']%%');

    /* Set location of settings file */
    define('PL_SETTINGS_FILE', pl_custom_directory() . '/config/settings.php');
    define('PL_DEFAULT_PREFS_FILE', pl_custom_directory() . '/config/default_prefs.php');

    // Initialize the connection to the MySQL server.
    if (!defined('PL_DISABLE_MYSQL')) {
        pl_mysql_init() or trigger_error('Could not connect to MySQL server.  Please check database connection settings and/or verify that an instance of MySQL is running on the specified host.  ERROR # ' . mysql_errno());
    }


    ini_set('session.use_cookies', '1');
    ini_set('session.use_only_cookies', '1');
    ini_set('session.use_trans_sid', '0');
    ini_set('session.hash_function', 'sha256'); // Stronger hashing algorithm
    ini_set('session.hash_bits_per_character', '5');
    ini_set('session.cookie_secure', '1');       // HTTPS only
    ini_set('session.cookie_httponly', '1');     // Prevent JavaScript access
    ini_set('session.cookie_samesite', 'Strict'); // Prevent CSRF
    ini_set('session.gc_maxlifetime', '1800');   // Session timeout: 30 minutes



    require_once('pikaSettings.php');
    $plSettings = pikaSettings::getInstance();

    // AMW - This will redirect the user to https:// if they connect over
    // http:// to a server that requires a secure connection.
    if (true == $plSettings['force_https'] && 0 == strlen($_SERVER['HTTPS'])) {
        header("Location: https://" . $_SERVER['SERVER_NAME'] .
            $_SERVER['REQUEST_URI']);
    }

    // GZIP compression
    if ($plSettings['enable_compression'] && !defined('PIKA_NO_COMPRESSION')) {
        ob_start("ob_gzhandler");
    }


    session_set_cookie_params(0, $plSettings['base_url']);

    // Set this to avoid other php websites (such as SugarCRM) from invading the current session w/ serialized objects
    $session_name = 'PikaCMS' . PIKA_VERSION . PIKA_REVISION . PIKA_PATCH_LEVEL;
    if (isset($plSettings['cookie_prefix']) && strlen($plSettings['cookie_prefix'])) { // Session Name only accepts letters and numbers so remove all non letters and/or numbers
        $session_name = preg_replace('/[^a-z0-9]/i', '', $plSettings['cookie_prefix']);
    }

    session_name($session_name);
    session_start();

    // Set server time zone, per PHP best practices.
    // AMW - 2013-02-20 - I moved this up, above authentication, because authentication
    // was using date() and causing warnings.
    $time_zone = pl_settings_get('time_zone');

    if (function_exists('date_default_timezone_set')) {
        if (!$time_zone) {
            $time_zone = 'America/New_York';
        }

        date_default_timezone_set($time_zone);
    }

    require_once('pikaAuth.php');

    // TODO - need to fix pikaAuth to be parent super-object over pikaAuthHttp
    //        until then will need to refer to auth object in context
    //        pikaAuthHttp in HTTP sections pikaAuth in all other sections
    if (defined('PL_DISABLE_SECURITY')) {
        $auth_row = pikaAuth::getInstance()->getAuthRow();
    } elseif (defined('PL_HTTP_SECURITY')) {
        authenticate_http();
        $auth_row = pikaAuthHttp::getInstance()->getAuthRow();
    } else {
        authenticate();
        $auth_row = pikaAuth::getInstance()->getAuthRow();
    }

    require_once('pikaDefPrefs.php');
    pikaDefPrefs::getInstance()->initPrefs($auth_row['user_id']);

    return true;
}
