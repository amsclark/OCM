<?php

/**********************************/

/* Pika CMS (C) 2008 Aaron Worley */
/* http://pikasoftware.com        */
/*                                */
/* Modified December 2019         */
/* By Metatheria, LLC             */
/* https://metatheria.solutions   */
/**********************************/

require_once('pika-danio.php');
pika_init();
require_once('pikaMisc.php');

$result = DB::preparedQuery("SELECT VERSION() AS mysql_version", []) or trigger_error();
$a = DBResult::fetchRow($result);
$a['pika_version'] = PIKA_VERSION;
$a['pika_revision'] = PIKA_REVISION;
$a['pika_patch_level'] = PIKA_PATCH_LEVEL;
$a['pika_code_name'] = PIKA_CODE_NAME;
$a['config_date'] = date('m/d/Y h:i A', filemtime(pl_custom_directory() . '/config/settings.php'));
$a['install_date'] = date('m/d/Y h:i A', filemtime('pika-danio.php'));
$a['server_name'] = $_SERVER['SERVER_NAME'];
$a['php_version'] = phpversion();
$a['apache_version'] = $_SERVER["SERVER_SOFTWARE"];

//diagnostic info to make sure server can accommodate larger file uploads
$a['maxUploadSize'] = ini_get('upload_max_filesize');
$a['postMaxSize'] = ini_get('post_max_size');
$result = DB::preparedQuery("SHOW VARIABLES LIKE 'max_allowed_packet'", array());
$row = DBResult::fetchRow($result);
$a['maxAllowedPacket'] = $row['Value'] / 1048576 . "M";

$a['base_url'] = $base_url = pl_settings_get('base_url');


// add in report list for sidebar
$reports = pikaMisc::reportList(true);
$y = "";
foreach ($reports as $z) {
    $y .= "<li>{$z}</li>\n";
}
$plTemplate['report_list'] = $y;

$plTemplate["content"] = pl_template($a, 'subtemplates/about.html');
$plTemplate["page_title"] = "About %%[branding]%% CMS";
$plTemplate['nav'] = "<a href=\"{$base_url}\">%%[branding]%% Home</a>
						&gt; <a href=\"{$base_url}/site_map.php\">Site Map</a>
						&gt; About %%[branding]%%";


$buffer = pl_template($plTemplate, 'templates/default.html');
pika_exit($buffer);
