<?php

/***********************************/

/* Pika CMS (C) 2002 Aaron Worley */
/* http://pikasoftware.com         */
/*                                 */
/* Modified December 2019          */
/* By Metatheria, LLC              */
/* https://metatheria.solutions    */
/***********************************/

require_once('pika-danio.php');

pika_init();

require_once('plFlexList.php');
require_once('pikaTempLib.php');
require_once('pikaDocument.php');
require_once('pikaMisc.php');

// FUNCTIONS


$main_html = $html = array();


$action = pl_grab_post('doc_upload');
$screen = pl_grab_post('description');
$user_id = pl_grab_post('doc_path');
$base_url = pl_settings_get('base_url');

$main_html['page_title'] = 'Form Letter Manager';


if (!pika_authorize("system", array())) {
    $main_html["content"] = "Access denied";
    $main_html["nav"] = "<a href=\"{$base_url}\">%%[branding]%% Home</a> &gt; {$main_html['page title']}";

    $default_template = new pikaTempLib('templates/default.html', $main_html);
    $buffer = $default_template->draw();
    pika_exit($buffer);
}

// add in report list for sidebar
$reports = pikaMisc::reportList(true);
$y = "";

foreach ($reports as $z) {
    $y .= "<li>{$z}</li>\n";
}

$main_html['report_list'] = $y;

$main_html['nav'] = "<a href=\"{$base_url}\">%%[branding]%% Home</a> &gt; {$main_html['page_title']}";
$template = new pikaTempLib('subtemplates/system-forms.html', $html);
$main_html['content'] = $template->draw();

$default_template = new pikaTempLib('templates/default.html', $main_html);
$buffer = $default_template->draw();
pika_exit($buffer);
