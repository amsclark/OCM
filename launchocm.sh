#!/bin/bash

# Start MySQL server
/usr/bin/mysqld_safe &

# Wait for the database service to be ready
until mysqladmin ping -h 127.0.0.1 --silent; do
  echo "Waiting for database connection..."
  sleep 2
done

# Start Apache HTTP server
/usr/sbin/httpd
sleep 10

# Check if the 'cms' database exists
RESULT=$(mysql -uroot -sN -e "SHOW DATABASES LIKE 'cms'")
if [ "$RESULT" == "cms" ]; then
  echo "Database already exists"
else
  echo "Database does not exist"
  mysql -uroot -e "CREATE DATABASE cms"
  cat /var/www/html/cms/app/sql/install/new_install.sql | mysql -uroot cms
  mysql -uroot -e "UPDATE users SET username='support', password=md5('password')" cms
  cp /var/www/html/cms-custom/config/settings.php.example /var/www/html/cms-custom/config/settings.php
fi

# Keep the script running
tail -f /dev/null
